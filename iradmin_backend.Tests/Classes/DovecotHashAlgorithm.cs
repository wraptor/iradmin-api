using System;
using Xunit;

using iradmin_backend;

namespace iradmin_backend.Tests
{
    public class DovecotHashAlgorithmTests
    {
        const string plain = "test";

        [Theory]
        [InlineData("{SHA1}qUqP5cyxm6YcTAhz05Hph5gvu9M=")]
        [InlineData("{SHA1.B64}qUqP5cyxm6YcTAhz05Hph5gvu9M=")]
        [InlineData("{SHA1.BASE64}qUqP5cyxm6YcTAhz05Hph5gvu9M=")]
        public async void Compare_encoding_Base64_validate(string hash)
        {           
            // Act
            var result = await DovecotHashAlgorithm.Compare(hash, plain);

            // Assert
            Assert.True(result, "Specified encoding type doesn't act as Base64");
        }

        [Fact]
        public async void Compare_encoding_Hex_validate()
        {
            // Act
            var result = await DovecotHashAlgorithm.Compare("{SHA1.HEX}a94a8fe5ccb19ba61c4c0873d391e987982fbbd3", plain);

            // Assert
            Assert.True(result, "Hex encoding type isn't handled as Hex");
        }

        [Fact]
        public async void Compare_encoding_Plain_validate()
        {
            // Act
            var result = await DovecotHashAlgorithm.Compare("{PLAIN}test", plain);

            // Assert
            Assert.True(result, "Plain/none encoding failed to validate");
        }

        [Fact]
        public async void Compare_hash_unsupported_validate()
        {
            try
            {
                // Act
                await DovecotHashAlgorithm.Compare("{123dseq}ghsadhajs", plain);
            }
            catch(Exception ex)
            {
                // Assert
                Assert.True((ex is NotSupportedException), "Invalid hash type doesn't throw NotSupportedException");
                Assert.True(ex.Source.StartsWith("Algorithm"), "Invalid hash type doesn't result in correct NotSupportedException response");
                return;
            }

            Assert.True(false, "Invalid hash type did compare successfully");
        }

        [Theory]
        [InlineData("{LDAP-MD5}CY9rzUYh03PK3k6DJie09g==")]
        [InlineData("{PLAIN-MD5}098f6bcd4621d373cade4e832627b4f6")]
        [InlineData("{SHA}qUqP5cyxm6YcTAhz05Hph5gvu9M=")]
        [InlineData("{SHA1}qUqP5cyxm6YcTAhz05Hph5gvu9M=")]
        [InlineData("{SHA256}n4bQgYhMfWWaL+qgxVrQFaO/TxsrC4Is0V1sFbDwCgg=")]
        [InlineData("{SHA512}7iaw3Ur350mqGo7jwQrpkj9hiYB3Lkc/iBml1JQODbJ6wYX4oOHV+E+IvIh/1nsUNzLDBMxfqa2Ob1f1ACio/w==")]

        [InlineData("{SMD5}lnIXWVfdymW2jy8+H+yIfZATDTc=")]
        [InlineData("{SSHA}8wG59iLylcoUj5VVzp0P3yfRzkOAOAuh")]
        [InlineData("{SSHA256}DZG9zKT+qnsrZyox7MpvZfPpFjexDZ3p0IR4sc+pGYJmF+n1")]
        [InlineData("{SSHA512}nE4u6QTKqZPJciE8YsiqWBNmHlornEGAL4pYx4NzjBwTfuQ8+2YnpPRxl8M7k/GhQXkT2F0HhMYZhuxgcJpkbdpJmyU=")]
        public async void Compare_hash_validate(string hash)
        {
            // Act
            var result = await DovecotHashAlgorithm.Compare(hash, plain);

            // Assert
            Assert.True(result, "Valid hash compared as invalid");
        }

        [Theory]
        [InlineData("{LDAP-MD5}none")]
        [InlineData("{PLAIN-MD5}none")]
        [InlineData("{SHA}none")]
        [InlineData("{SHA1}none")]
        [InlineData("{SHA256}none")]
        [InlineData("{SHA512}none")]

        [InlineData("{SMD5}none")]
        [InlineData("{SSHA}none")]
        [InlineData("{SSHA256}none")]
        [InlineData("{SSHA512}none")]
        public async void Compare_hash_invalidate(string hash)
        {
            // Act
            var result = await DovecotHashAlgorithm.Compare(hash, plain);

            // Assert
            Assert.False(result, "Invalid has compared as valid");
        }
    }
}