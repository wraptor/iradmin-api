using System;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Xunit;

using iradmin_backend.Controllers;
using iradmin_backend.Models;

namespace iradmin_backend.Tests.Controllers
{
    public class DomainControllerTests : IDisposable
    {
        private Contexts.vmailContext dbContext;
        private readonly ILogger<DomainController> _log = General.GetLogger<DomainController>();

        public DomainControllerTests()
        {
            dbContext = new Contexts.vmailContext(new DbContextOptionsBuilder<Contexts.vmailContext>()
                    .UseInMemoryDatabase(databaseName: $"vmail_domains_{Guid.NewGuid()}")
                    .Options);

            dbContext.Database.EnsureCreated();
            
            // Testing data
            dbContext.Domain.AddRange(new Domain[] {
                new Domain() { Domainname = "fake.tld", Active = 1, },
                new Domain() { Domainname = "otherfake.tld", Active = 1 },
            });

            dbContext.SaveChanges();
        }

        [Theory]
        [InlineData("fake.tld", 2)]
        [InlineData("otherfake.tld", 2)]
        public async void GetDomains_superadmin_validate(string domain, int role)
        {
            // Arrange
            DomainController controller = new DomainController(dbContext, _log);
            controller.ControllerContext = new ControllerContext();
            controller.ControllerContext.HttpContext = new DefaultHttpContext();
            controller.ControllerContext.HttpContext.Items[Constants.General.Domain] = domain;
            controller.ControllerContext.HttpContext.Items[Constants.General.Role] = role;

            // Act
            var viewResult = Assert.IsType<OkObjectResult>(await controller.GetDomains());
            var responseObject = Assert.IsAssignableFrom<Domain[]>(viewResult.Value);

            // Assert
            Assert.Equal(200, viewResult.StatusCode);
            Assert.Equal(await dbContext.Domain.CountAsync(), responseObject.Length);
        }

        [Theory]
        [InlineData("fake.tld", 1)]
        [InlineData("otherfake.tld", 1)]
        public async void GetDomains_admin_validate(string domain, int role)
        {
            // Arrange
            DomainController controller = new DomainController(dbContext, _log);
            controller.ControllerContext = new ControllerContext();
            controller.ControllerContext.HttpContext = new DefaultHttpContext();
            controller.ControllerContext.HttpContext.Items[Constants.General.Domain] = domain;
            controller.ControllerContext.HttpContext.Items[Constants.General.Role] = role;

            // Act
            var viewResult = Assert.IsType<OkObjectResult>(await controller.GetDomains());
            var responseObject = Assert.IsAssignableFrom<Domain[]>(viewResult.Value);
        }

        [Fact]
        public async void GetDomains_user_invalidate()
        {
            // Arrange
            DomainController controller = new DomainController(dbContext, _log);
            controller.ControllerContext = new ControllerContext();
            controller.ControllerContext.HttpContext = new DefaultHttpContext();
            controller.ControllerContext.HttpContext.Items[Constants.General.Domain] = "fake.tld";
            controller.ControllerContext.HttpContext.Items[Constants.General.Role] = 0;

            // Act
            var result = await controller.GetDomains();

            // Assert
            Assert.IsType<UnauthorizedResult>(result);
        }

        [Fact]
        public async void GetDomains_invalidate()
        {
            // Arrange
            DomainController controller = new DomainController(dbContext, _log);
            controller.ControllerContext = new ControllerContext();
            controller.ControllerContext.HttpContext = new DefaultHttpContext();

            // Act
            var result = await controller.GetDomains();

            // Assert
            Assert.IsType<UnauthorizedResult>(result);
        }

        [Theory]
        [InlineData("fake.tld", 2)]
        [InlineData("fake.tld", 1)]
        public async void GetDomains_empty(string domain, int role)
        {
            // Arrange
            var dbContext = General.GetEmptyVmailContext();

            DomainController controller = new DomainController(dbContext, _log);
            controller.ControllerContext = new ControllerContext();
            controller.ControllerContext.HttpContext = new DefaultHttpContext();
            controller.ControllerContext.HttpContext.Items[Constants.General.Domain] = domain;
            controller.ControllerContext.HttpContext.Items[Constants.General.Role] = role;

            // Act
            var result = await controller.GetDomains();

            // Assert
            Assert.IsType<NoContentResult>(result);
        }

        [Theory]
        [InlineData("fake.tld", 2, "otherfake.tld")]
        [InlineData("fake.tld", 1, "fake.tld")]
        public async void GetDomain_validate(string domain, int role, string domainToRequest)
        {
            // Arrange
            DomainController controller = new DomainController(dbContext, _log);
            controller.ControllerContext = new ControllerContext();
            controller.ControllerContext.HttpContext = new DefaultHttpContext();
            controller.ControllerContext.HttpContext.Items[Constants.General.Domain] = domain;
            controller.ControllerContext.HttpContext.Items[Constants.General.Role] = role;

            // Act
            var viewResult = Assert.IsType<OkObjectResult>(await controller.GetDomain(domainToRequest));
            var responseObject = Assert.IsAssignableFrom<Domain>(viewResult.Value);

            // Assert
            Assert.Equal(200, viewResult.StatusCode);
            Assert.Equal(domainToRequest, responseObject.Domainname);
        }

        [Theory]
        [InlineData(null, 0, "otherfake.tld")]
        [InlineData("fake.tld", 1, "otherfake.tld")]
        public async void GetDomain_invalidate(string domain, int role, string domainToRequest)
        {
            // Arrange
            DomainController controller = new DomainController(dbContext, _log);
            controller.ControllerContext = new ControllerContext();
            controller.ControllerContext.HttpContext = new DefaultHttpContext();
            controller.ControllerContext.HttpContext.Items[Constants.General.Domain] = domain;
            controller.ControllerContext.HttpContext.Items[Constants.General.Role] = role;

            // Act
            var result = await controller.GetDomain(domainToRequest);

            // Assert
            Assert.IsType<UnauthorizedResult>(result);
        }
        
        [Fact]
        public async void GetDomain_empty()
        {
            // Arrange
            var dbContext = General.GetEmptyVmailContext();
            
            DomainController controller = new DomainController(dbContext, _log);
            controller.ControllerContext = new ControllerContext();
            controller.ControllerContext.HttpContext = new DefaultHttpContext();
            controller.ControllerContext.HttpContext.Items[Constants.General.Domain] = "fake.tld";
            controller.ControllerContext.HttpContext.Items[Constants.General.Role] = 2;

            // Act
            var result = await controller.GetDomain("fake.tld");

            // Assert
            Assert.IsType<NoContentResult>(result);
        }

        [Theory]
        [InlineData("fake.tld")]
        [InlineData("otherfake.tld")]
        public async void UpdateDomain_admin_validate(string domain)
        {
            // Arrange
            DomainController controller = new DomainController(dbContext, _log);
            controller.ControllerContext = new ControllerContext();
            controller.ControllerContext.HttpContext = new DefaultHttpContext();
            controller.ControllerContext.HttpContext.Items[Constants.General.Domain] = "fake.tld";
            controller.ControllerContext.HttpContext.Items[Constants.General.Role] = 1;

            // Act
            var domainBlob = dbContext.Domain.First(x => x.Domainname.ToLower() == domain);

            domainBlob.Domainname = domain;
            domainBlob.Description = "testing a sentence";
            domainBlob.Disclaimer = "testing a sentence";
            domainBlob.Transport = "not_dovecot";
            var result = Assert.IsType<OkResult>(await controller.UpdateDomain(domain, domainBlob));

            var resultDomain = dbContext.Domain.First(x => x.Domainname.ToLower() == domain);

            // Assert
            Assert.Equal(domainBlob, resultDomain);
        }

        
        [Fact]
        public async void UpdateDomain_admin_invalidate()
        {
            // Arrange
            DomainController controller = new DomainController(dbContext, _log);
            controller.ControllerContext = new ControllerContext();
            controller.ControllerContext.HttpContext = new DefaultHttpContext();
            controller.ControllerContext.HttpContext.Items[Constants.General.Domain] = "fake.tld";
            controller.ControllerContext.HttpContext.Items[Constants.General.Role] = 1;

            // Act
            var result = await controller.UpdateDomain("invalid", null);

            // Assert
            Assert.IsType<BadRequestResult>(result);
        }


        public void Dispose()
        {
            dbContext.Database.EnsureDeleted();

            dbContext.Dispose();
        }
    }
}