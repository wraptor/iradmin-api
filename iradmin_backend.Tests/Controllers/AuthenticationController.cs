using System;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Xunit;

using iradmin_backend.Controllers;
using iradmin_backend.Models;

namespace iradmin_backend.Tests.Controllers
{
    public class AuthenticationControllerTests : IDisposable
    {
        private readonly Contexts.vmailContext dbContext;
        private readonly ILogger<AuthenticationController> _log = General.GetLogger<AuthenticationController>();

        public AuthenticationControllerTests()
        {
            dbContext = new Contexts.vmailContext(new DbContextOptionsBuilder<Contexts.vmailContext>()
                    .UseInMemoryDatabase(databaseName: $"vmail_authentication_{Guid.NewGuid()}")
                    .Options);

            dbContext.Database.EnsureCreated();
            
            // Testing data
            dbContext.Mailbox.AddRange(new Mailbox[] {
                new Mailbox() { Username = "superadmin@fake.tld", Domain = "fake.tld", Password = "{SSHA512}nE4u6QTKqZPJciE8YsiqWBNmHlornEGAL4pYx4NzjBwTfuQ8+2YnpPRxl8M7k/GhQXkT2F0HhMYZhuxgcJpkbdpJmyU=", Active = 1 },
                new Mailbox() { Username = "admin@fake.tld", Domain = "fake.tld", Password = "{SHA1}qUqP5cyxm6YcTAhz05Hph5gvu9M=", Active = 1 },
                new Mailbox() { Username = "disabled@fake.tld", Domain = "fake.tld", Password = "{SSHA512}nE4u6QTKqZPJciE8YsiqWBNmHlornEGAL4pYx4NzjBwTfuQ8+2YnpPRxl8M7k/GhQXkT2F0HhMYZhuxgcJpkbdpJmyU=", Active = 0 },
                new Mailbox() { Username = "user@unsupported.tld", Domain = "unsupported.tld", Password = "{asd}$1$IFhMuMF9$J5DhpRDhS51CDqiaOwiw3.", Active = 1 }
            });

            dbContext.SaveChanges();
        }

        [Theory]
        [InlineData("superadmin@fake.tld", "test")]
        [InlineData("admin@fake.tld", "test")]

        public async void Login_validate(string email, string password)
        {
            // Arrange
            AuthenticationController controller = new AuthenticationController(dbContext, _log);
            controller.ControllerContext = new ControllerContext();
            controller.ControllerContext.HttpContext = new DefaultHttpContext();
            controller.ControllerContext.HttpContext.Session = General.GetSession();
            
            // Act
            var result = await controller.Login(new AuthRequest() { Mail = email, Password = password });
             
            // Assert
            Assert.IsType<OkResult>(result);
        }

        [Theory]
        [InlineData("superadmin@fake.tld", "badpw")]
        [InlineData("superadmin@fake.tld", "t3st")]

        public async void Login_invalidate(string email, string password)
        {
            // Arrange
            AuthenticationController controller = new AuthenticationController(dbContext, _log);
            controller.ControllerContext = new ControllerContext();
            controller.ControllerContext.HttpContext = new DefaultHttpContext();
            controller.ControllerContext.HttpContext.Session = General.GetSession();
            
            // Act
            var result = await controller.Login(new AuthRequest() { Mail = email, Password = password });
             
            // Assert
            Assert.IsType<UnauthorizedResult>(result);
        }

        [Theory]
        [InlineData("superadmin@fake.tld", "")]
        [InlineData("superadmin@fake.tld", null)]
        [InlineData("", "test")]
        [InlineData(null, "test")]
        [InlineData("", "")]
        [InlineData(null, null)]
        public async void Login_badrequest(string email, string password)
        {
            // Arrange
            AuthenticationController controller = new AuthenticationController(dbContext, _log);
            controller.ControllerContext = new ControllerContext();
            controller.ControllerContext.HttpContext = new DefaultHttpContext();
            controller.ControllerContext.HttpContext.Session = General.GetSession();
            
            // Act
            var result = await controller.Login(new AuthRequest() { Mail = email, Password = password });
             
            // Assert
            Assert.IsType<BadRequestObjectResult>(result);
        }

        [Fact]
        public async void Login_nosession_Check()
        {
            // Arrange
            AuthenticationController controller = new AuthenticationController(dbContext, _log);
            controller.ControllerContext = new ControllerContext();
            controller.ControllerContext.HttpContext = new DefaultHttpContext();
            
            // Act
            var result = await controller.Login(new AuthRequest() { Mail = "superadmin@fake.tld", Password = "test" });

            // Assert
            var statusObject = Assert.IsType<StatusCodeResult>(result);
            Assert.Equal(500, statusObject.StatusCode);
        }

        [Fact]
        public async void Login_invalidpassalgorithm_Check()
        {
            // Arrange
            AuthenticationController controller = new AuthenticationController(dbContext, _log);
            controller.ControllerContext = new ControllerContext();
            controller.ControllerContext.HttpContext = new DefaultHttpContext();
            controller.ControllerContext.HttpContext.Session = General.GetSession();
            
            // Act
            var result = await controller.Login(new AuthRequest() { Mail = "user@unsupported.tld", Password = "test" });

            // Assert
            var statusObject = Assert.IsType<StatusCodeResult>(result);
            Assert.Equal(500, statusObject.StatusCode);
        }


        public void Dispose()
        {
            dbContext.Database.EnsureDeleted();

            dbContext.Dispose();
        }
    }
}
