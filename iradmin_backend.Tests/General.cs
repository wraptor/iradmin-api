using System;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Moq;

using iradmin_backend.Contexts;
using iradmin_backend.Models;

namespace iradmin_backend.Tests
{
    internal class General
    {
        internal static ILogger<T> GetLogger<T>()
        {
            return Mock.Of<ILogger<T>>();
        }

        internal static ISession GetSession()
        {
            return Mock.Of<ISession>();
        }

        internal static vmailContext GetEmptyVmailContext()
        {
            return new vmailContext(new DbContextOptionsBuilder<Contexts.vmailContext>()
                    .UseInMemoryDatabase(databaseName: $"vmail_empty_{Guid.NewGuid()}")
                    .Options);
        }
    }
}