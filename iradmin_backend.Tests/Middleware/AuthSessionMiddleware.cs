using System;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Xunit;

using iradmin_backend.Middleware;
using iradmin_backend.Contexts;
using iradmin_backend.Models;

namespace iradmin_backend.Tests.Middleware
{
    public class AuthSessionMiddlewareTests : IDisposable
    {
        private readonly vmailContext dbContext;

        public AuthSessionMiddlewareTests()
        {
            dbContext = new Contexts.vmailContext(new DbContextOptionsBuilder<Contexts.vmailContext>().UseInMemoryDatabase(databaseName: $"vmail_authsessionmiddleware_{Guid.NewGuid()}").Options);
            
            dbContext.Database.EnsureCreated();
            dbContext.Mailbox.AddRange(new Mailbox[] {
                new Mailbox() { Username = "superadmin@fake.tld", Domain = "fake.tld", Password = "{SSHA512}nE4u6QTKqZPJciE8YsiqWBNmHlornEGAL4pYx4NzjBwTfuQ8+2YnpPRxl8M7k/GhQXkT2F0HhMYZhuxgcJpkbdpJmyU=", Active = 1, Isadmin = 1, Isglobaladmin = 1, Expired = DateTime.MaxValue },
                new Mailbox() { Username = "admin@fake.tld", Domain = "fake.tld", Password = "{SSHA512}nE4u6QTKqZPJciE8YsiqWBNmHlornEGAL4pYx4NzjBwTfuQ8+2YnpPRxl8M7k/GhQXkT2F0HhMYZhuxgcJpkbdpJmyU=", Active = 1, Isadmin = 1, Isglobaladmin = 0, Expired = DateTime.MaxValue },
                new Mailbox() { Username = "expired@fake.tld", Domain = "fake.tld", Password = "{SSHA512}nE4u6QTKqZPJciE8YsiqWBNmHlornEGAL4pYx4NzjBwTfuQ8+2YnpPRxl8M7k/GhQXkT2F0HhMYZhuxgcJpkbdpJmyU=", Active = 1, Isadmin = 0, Isglobaladmin = 0, Expired = DateTime.MinValue }
            });

            dbContext.SaveChanges();
        }

        [Theory]
        [InlineData("superadmin@fake.tld", 2)]
        [InlineData("admin@fake.tld", 1)]
        public async void InvokeAsync_validate(string email, int role)
        {
#pragma warning disable
            // Arrange
            RequestDelegate reqDelegate = async (responseHttpContext) => {
                // Assert
                Assert.Equal("fake.tld", responseHttpContext.Items[Constants.General.Domain]);
                Assert.Equal(role, responseHttpContext.Items[Constants.General.Role]);
            };
#pragma warning restore
            AuthSessionMiddleware middleware = new AuthSessionMiddleware(reqDelegate, General.GetLogger<AuthSessionMiddleware>());
            DefaultHttpContext httpContext = new DefaultHttpContext();
            httpContext.Session = new Mocks.MockHttpSession();
            httpContext.Session.SetString(Constants.Session.Mail, email);

            // Act
            await middleware.InvokeAsync(httpContext, dbContext);
        }

        [Theory]
        [InlineData("invalid@fake.tld")]
        [InlineData("expired@fake.tld")]
        public async void InvokeAsync_invalidate(string email)
        {
#pragma warning disable
            // Arrange
            RequestDelegate reqDelegate = async (responseHttpContext) => {
                // Assert
                Assert.Null(responseHttpContext.Items[Constants.General.Domain]);
                Assert.Null(responseHttpContext.Items[Constants.General.Role]);

                Assert.Null(responseHttpContext.Session.GetString(Constants.Session.Mail));
            };
#pragma warning restore
            AuthSessionMiddleware middleware = new AuthSessionMiddleware(reqDelegate, General.GetLogger<AuthSessionMiddleware>());
            DefaultHttpContext httpContext = new DefaultHttpContext();
            httpContext.Session = new Mocks.MockHttpSession();
            httpContext.Session.SetString(Constants.Session.Mail, email);

            // Act
            await middleware.InvokeAsync(httpContext, dbContext);
        }

        public void Dispose()
        {
            dbContext.Database.EnsureDeleted();

            dbContext.Dispose();
        }
    }
}