<!---
Please read this!

Before opening a new issue, make sure to search for keywords in the issues
filtered by the "regression" or "bug" label.

- https://gitlab.wheezie.be/iradmin/restful-webapi/issues?label_name%5B%5D=feature

and verify the feature you're about to propose isn't a duplicate.
--->

# Feature request
### Description

(Describe the feature, what it does etc.)

### Desired functionality

(Should the feature be manipulated using eg. a select-box or something else? Define how you'd like it to be implemented)

### Possible solution

(If you can, link to the line of code that might be responsible for the problem)

/label ~feature