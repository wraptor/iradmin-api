<!---
Please read this!

Before opening a new issue, make sure to search for keywords in the issues
filtered by the "regression" or "bug" label.

- https://gitlab.wheezie.be/iradmin/restful-webapi/issues?label_name%5B%5D=bug

and verify the bug you're about to submit isn't a duplicate.
--->
# Bug report

### Summary

(Summarize the bug encountered concisely)

### Steps to reproduce

(How one can reproduce the issue - this is very important)

### What is the current *bug* behavior?

(What actually happens)

### What is the expected *correct* behavior?

(What should happen instead)

### Relevant logs and/or screenshots

(Paste any relevant logs - please use these code blocks (```) to format console output,
logs, and code as it's very hard to read otherwise.)


### Possible fixes

(Provide possible fixes you might know, perhaps some code )

/label ~bug