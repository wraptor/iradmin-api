﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace iradmin_backend
{
    /// <summary>
    /// Algorithm class provides Crypto-Hashing and comparing for the Dovecot schema
    /// </summary>
    public static class DovecotHashAlgorithm
    {
        /// <summary>
        /// Compare a hashed string to a given password
        /// </summary>
        /// <param name="hashedString">Dovecot-structured hash-string</param>
        /// <param name="passwordString">An UTF8-based password </param>
        /// <returns>Returns the async result of the comparison</returns>
        public static async Task<bool> Compare(string hashedString, string passwordString)
        {
            string[] splitString = hashedString.Split('}');
            splitString[0] = splitString[0].Replace("{", string.Empty).ToUpper();

            string strAlgorithmType = splitString[0];
            bool isHex = false;
            if (strAlgorithmType.Contains("."))
            {
                string[] prefixSubset = strAlgorithmType.Split('.');
                if (!string.IsNullOrWhiteSpace(prefixSubset[1])) {
                    switch (prefixSubset[1])
                    {
                        case Constants.HashAlgorithms.Base64:
                        case Constants.HashAlgorithms.Base64Short:
                            splitString[1] = await decodeBase64ToHex(splitString[1]);
                            isHex = true;
                            break;
                        case Constants.HashAlgorithms.HEXIdentifier:
                            isHex = true;
                            break;
                    }
                }

                strAlgorithmType = prefixSubset[0];
            }

            AlgorithmType algorithmType = null;
            if (Constants.HashAlgorithms.Algorithms.ContainsKey(strAlgorithmType))
            {
                algorithmType = Constants.HashAlgorithms.Algorithms[strAlgorithmType];
            }
            else if (!string.Equals(strAlgorithmType, Constants.HashAlgorithms.PlainText, StringComparison.Ordinal))
            {
                throw new NotSupportedException() { Source = $"Algorithm: {strAlgorithmType}" }; // Throw exception if algorithm unsupported
            }


            if (algorithmType == null)
            {
                return string.Equals(splitString[1], passwordString, StringComparison.Ordinal);
            }


            switch (algorithmType.Encoding)
            {
                case Constants.HashAlgorithms.Encoding.Base64:
                    if(!isHex)
                    {
                        splitString[1] = await decodeBase64ToHex(splitString[1]);
                    }
                    break;
                case Constants.HashAlgorithms.Encoding.Crypt:
                    string[] mcfSchema = splitString[1].Split('$');
                    if (!Constants.HashAlgorithms.MCFKeys.ContainsKey(mcfSchema[1]))
                    {
                        throw new NotSupportedException() { Source = $"MCF ID: {mcfSchema}" }; // Needs implementation
                    }
                    
                    break;
                case Constants.HashAlgorithms.Encoding.Hex:
                    break;
                default:
                    throw new NotSupportedException() { Source = $"Encoding ID: {algorithmType.Encoding}"  }; // Needs implementation
            }

            byte[] salt = null;
            int iLength = (splitString[1].Length - algorithmType.Length);
            if (iLength > 0)
            {
                if (algorithmType.SaltPrefixed)
                {
                    salt = await hexToArray(splitString[1].Substring(0, iLength));
                    splitString[1] = splitString[1].Substring(iLength, algorithmType.Length);
                }
                else
                {
                    salt = await hexToArray(splitString[1].Substring(algorithmType.Length, iLength));
                    splitString[1] = splitString[1].Substring(0, algorithmType.Length);
                }
            }

            List<byte> list = new List<byte>();
            list.AddRange(System.Text.Encoding.UTF8.GetBytes(passwordString));
            if (salt != null && salt.Length > 0)
            {
                if (algorithmType.SaltPrefixed)
                {
                    list.InsertRange(0, salt);
                }
                else
                {
                    list.AddRange(salt);
                }
            }

            string resultPassword = BitConverter.ToString(algorithmType.Algorithm.ComputeHash(list.ToArray<byte>())).Replace("-", "");
            return string.Equals(resultPassword, splitString[1], StringComparison.OrdinalIgnoreCase);
        }

        private static async Task<string> decodeBase64ToHex(string inputBase64)
        {
            return await Task.Run(
                () => {
                    return BitConverter.ToString( Convert.FromBase64String(inputBase64) )
                        .Replace("-", "");
                });
        }

        private static async Task<string> decodeUtf8ToHex(string inputUtf8)
        {
            return await Task.Run(
                () => {
                    return BitConverter.ToString( System.Text.Encoding.UTF8.GetBytes(inputUtf8) )
                        .Replace("-", "");
                });
        }

        private static async Task<byte[]> hexToArray(string inputHex)
        {
            return await Task.Run(
                () => {
                    return Enumerable.Range(0, inputHex.Length)
                             .Where(x => x % 2 == 0)
                             .Select(x => Convert.ToByte(inputHex.Substring(x, 2), 16))
                             .ToArray();
                });
        }
    }
}