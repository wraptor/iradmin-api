namespace iradmin_backend
{
    public static class General
    {
        internal static string TrimLength(string input, int maxlength)
        {
            if(input.Length > maxlength)
            {
                return input.Substring(0, (maxlength-1));
            }
            return input;
        }
    }
}