using System;

namespace iradmin_backend.Classes.Responses
{
    public class DomainsResponse
    {
        public PreviewDomain[] Domains { get; set; }
    }

    public class PreviewDomain
    {
        public string Domain { get; set; }
        public bool Active { get; set; }
        public long Quota { get; set; }
        public DateTime Expired { private get; set; }
        public bool IsExpired => (Expired <= DateTime.UtcNow);
    }
}