﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace iradmin_backend.Classes
{
    public class HandledException : Exception
    {

        public uint Result { get; private set; }

        public string LogText { get; set; }

        public HandledException(uint code, string message)
        {
            Result = code;
            LogText = message;
        }

        public string ToJsonString()
        {
            return string.Format(Constants.Responses.JsonErrorResponse, Result);
        }
    }
}
