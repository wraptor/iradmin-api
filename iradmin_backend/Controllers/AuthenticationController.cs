﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

using iradmin_backend.Contexts;

namespace iradmin_backend.Controllers
{
    [Route("auth")]
    [ApiController]
    public class AuthenticationController : Controller
    {
        private readonly vmailContext vMailContext;
        private readonly ILogger<AuthenticationController> Logger;

        public AuthenticationController(vmailContext vmailContext, ILogger<AuthenticationController> logger)
        {
            vMailContext = vmailContext;
            Logger = logger;
        }

        [HttpPost("login")]
        public async Task<IActionResult> Login([FromBody]AuthRequest authRequest)
        {
            // Check if logged in already
            if(HttpContext.Items.ContainsKey(Constants.General.Role)
            && HttpContext.Items.ContainsKey(Constants.General.Domain))
            {
                return Ok();
            }

            if (string.IsNullOrWhiteSpace(authRequest.Mail))
            {
                return BadRequest(Constants.Responses.MailRequired);
            }
            authRequest.Mail = authRequest.Mail.ToLower();

            if (string.IsNullOrWhiteSpace(authRequest.Password))
            {
                return BadRequest(Constants.Responses.PasswordRequired);
            }
            string passHash = await (from x in vMailContext.Mailbox
                                        where x.Username.ToLower() == authRequest.Mail && x.Active == 1
                                        select x.Password).FirstAsync();
            if (passHash != null)
            {
                bool bValid = false;
                try
                {
                    bValid = await DovecotHashAlgorithm.Compare(passHash, authRequest.Password);
                }
                catch (NotSupportedException ex)
                {
                    Logger.LogError(ex, "Hash comparing exception for user: {Mail}", authRequest.Mail);
                    return StatusCode(500);
                }

                if (bValid)
                {
                    try
                    {
                        await HttpContext.Session.LoadAsync();
                        HttpContext.Session.SetString(Constants.Session.Mail, authRequest.Mail);
                        await HttpContext.Session.CommitAsync();
                    }
                    catch (Exception ex)
                    {
                        Logger.LogError(ex, "Session exception");
                        return StatusCode(500);
                    }

                    Logger.LogInformation("Auth success: {Mail}, session: {Session}, ip: {RemoteIpAddress}", authRequest.Mail, HttpContext.Session.Id, HttpContext.Connection.RemoteIpAddress);
                    return Ok();
                }
            }

            Logger.LogInformation("Auth failed: {Mail}, ip: {RemoteIpAddress}", authRequest.Mail, HttpContext.Connection.RemoteIpAddress);
            return Unauthorized();
        }

        [HttpPost("logout")]
        public async Task<IActionResult> Logout()
        {
            var mailAddr = HttpContext.Session.GetString(Constants.Session.Mail);
            if(!string.IsNullOrWhiteSpace(mailAddr))
            {
                Logger.LogInformation("Logout: {Mail}, session: {Session} ip: {RemoteIpAddress}", mailAddr, HttpContext.Session.Id, HttpContext.Connection.RemoteIpAddress);
            }

            HttpContext.Session.Clear();
            await HttpContext.Session.CommitAsync();

            Response.Cookies.Delete(Constants.Session.Key);

            return Ok();
        }
    }

    public class AuthRequest
    {
        public string Mail { get; set; }
        public string Password { get; set; }
    }
}