using System;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;

using iradmin_backend.Classes.Responses;
using iradmin_backend.Contexts;
using iradmin_backend.Models;

namespace iradmin_backend.Controllers
{
    [ApiController]
    public class DomainController : Controller
    {
        private readonly vmailContext _vmailContext;
        private readonly ILogger<DomainController> _log;

        public DomainController(vmailContext vmailContext, ILogger<DomainController> log)
        {
            _vmailContext = vmailContext;
            _log = log;
        }

        [HttpGet]
        [Route("domains")]
        public async Task<IActionResult> GetDomains()
        {
            if(!IsAuthenticated(1))
            {
                return Unauthorized();
            }
            
            try {
                switch(HttpContext.Items[Constants.General.Role])
                {
                    case 1:
                        var domain = await (from x in _vmailContext.Domain
                            where x.Domainname.ToLower() == (string)HttpContext.Items[Constants.General.Domain]
                            select x).FirstAsync();
                        if(domain != null)
                            return Ok(new Domain[] { domain });
                        break;
                    case 2:
                        var domains = await (from x in _vmailContext.Domain
                            orderby x.Domainname ascending, x.Active descending
                            select x).ToArrayAsync();

                        if(domains != null && domains.Length > 0)
                            return Ok(domains);
                        break;
                }
            }
            catch(InvalidOperationException) { }

            return NoContent();
        }

        [HttpGet]
        [Route("domain/{domain}")]
        public async Task<IActionResult> GetDomain(string domain)
        {
            if(!IsAuthorized(domain))
            {
                return Unauthorized();
            }

            try {
                var Domain = await (from x in _vmailContext.Domain
                    where x.Domainname.ToLower() == domain.ToLower()
                    select x).FirstAsync();
                
                if(Domain != null)
                    return Ok(Domain);
            }
            catch(System.InvalidOperationException) { }

            return NoContent();
        }

        [HttpPatch]
        [Route("domain/{domain}")]
        public async Task<IActionResult> UpdateDomain(string domain, [FromBody]Domain Changes)
        {
            if(!IsAuthenticated(1))
            {
                return Unauthorized();
            }

            domain = domain.ToLower();
            Domain retrievedDomain;
            try
            {
                retrievedDomain = _vmailContext.Domain.First(x => x.Domainname.ToLower() == domain);
            }
            catch(InvalidOperationException)
            {
                return BadRequest();
            }
            catch(Exception ex)
            {
                _log.LogError(ex, "Database domain retrieval failed");
                return StatusCode(500);
            }

            retrievedDomain = GetFilteredDomain(retrievedDomain, Changes);

            _vmailContext.Domain.Update(retrievedDomain);

            try
            {
                await _vmailContext.SaveChangesAsync().ConfigureAwait(false);
            }
            catch(Exception ex)
            {
                _log.LogError(ex, "Database update failed");
                return StatusCode(500);
            }

            return Ok();
        }

        private Domain GetFilteredDomain(Domain original, Domain input)
        {
            original.Modified = DateTime.UtcNow;

            if(input.Description != null)
            {
                if(!string.IsNullOrWhiteSpace(input.Description))
                {
                    original.Description = General.TrimLength(input.Description, 65535);
                }
                else
                {
                    original.Description = null;
                }
            }

            if(input.Disclaimer != null)
            {
                if(!string.IsNullOrWhiteSpace(input.Disclaimer))
                {
                    original.Description = General.TrimLength(input.Description, 65535);
                }
                else
                {
                    original.Disclaimer = null;
                }
            }

            if(input.Transport != null)
            {
                if(!string.IsNullOrWhiteSpace(input.Transport))
                {
                    original.Transport = General.TrimLength(input.Transport, 255);
                }
                else
                {
                    original.Transport = "dovecot";
                }
            }

            if(!IsAuthenticated(2))
            {
                return original;
            }

            if(input.Aliases != null)
            {
                original.Aliases = input.Aliases;
            }
            if(input.Mailboxes != null)
            {
                original.Mailboxes = input.Mailboxes;
            }
            if(input.Maillists != null)
            {
                original.Maillists = input.Maillists;
            }
            if(input.Maxquota != null)
            {
                original.Maxquota = input.Maxquota;
            }

            if(original.Aliases < -1)
            {
                original.Aliases = -1;
            }
            if(original.Mailboxes < -1)
            {
                original.Mailboxes = -1;
            }
            if(original.Maillists < -1)
            {
                original.Maillists = -1;
            }
            if(original.Maxquota < -1)
            {
                original.Maxquota = -1;
            }

            return original;
        }

        private bool IsAuthenticated(int level = 0)
        {
            return HttpContext.Items.ContainsKey(Constants.General.Role)
            && HttpContext.Items.ContainsKey(Constants.General.Domain)
            && ((int)HttpContext.Items[Constants.General.Role] >= level);
        }

        private bool IsAuthorized(string domain = null)
        {
            return HttpContext.Items.ContainsKey(Constants.General.Role)
            && HttpContext.Items.ContainsKey(Constants.General.Domain)
            && (((int)HttpContext.Items[Constants.General.Role] >= 2) || string.Equals((string)HttpContext.Items[Constants.General.Domain], domain, System.StringComparison.OrdinalIgnoreCase));
        }
    }
}