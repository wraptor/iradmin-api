﻿using System;
using System.Collections.Generic;

namespace iradmin_backend.Models
{
    public partial class GreylistingTracking
    {
        public long Id { get; set; }
        public string Sender { get; set; }
        public string Recipient { get; set; }
        public string ClientAddress { get; set; }
        public string SenderDomain { get; set; }
        public string RcptDomain { get; set; }
        public int InitTime { get; set; }
        public int BlockExpired { get; set; }
        public int RecordExpired { get; set; }
        public long BlockedCount { get; set; }
        public byte Passed { get; set; }
    }
}
