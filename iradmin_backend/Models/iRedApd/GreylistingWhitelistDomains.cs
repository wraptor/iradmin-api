﻿using System;
using System.Collections.Generic;

namespace iradmin_backend.Models
{
    public partial class GreylistingWhitelistDomains
    {
        public long Id { get; set; }
        public string Domain { get; set; }
    }
}
