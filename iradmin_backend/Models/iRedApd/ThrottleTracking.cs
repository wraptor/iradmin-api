﻿using System;
using System.Collections.Generic;

namespace iradmin_backend.Models
{
    public partial class ThrottleTracking
    {
        public long Id { get; set; }
        public long Tid { get; set; }
        public string Account { get; set; }
        public int Period { get; set; }
        public int CurMsgs { get; set; }
        public int CurQuota { get; set; }
        public int InitTime { get; set; }
        public int LastTime { get; set; }

        public Throttle T { get; set; }
    }
}
