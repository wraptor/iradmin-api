﻿using System;
using System.Collections.Generic;

namespace iradmin_backend.Models
{
    public partial class WblistRdns
    {
        public long Id { get; set; }
        public string Rdns { get; set; }
        public string Wb { get; set; }
    }
}
