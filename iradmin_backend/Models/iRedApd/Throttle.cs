﻿using System;
using System.Collections.Generic;

namespace iradmin_backend.Models
{
    public partial class Throttle
    {
        public Throttle()
        {
            ThrottleTracking = new HashSet<ThrottleTracking>();
        }

        public long Id { get; set; }
        public string Account { get; set; }
        public string Kind { get; set; }
        public byte Priority { get; set; }
        public int Period { get; set; }
        public long MsgSize { get; set; }
        public long MaxMsgs { get; set; }
        public long MaxQuota { get; set; }

        public ICollection<ThrottleTracking> ThrottleTracking { get; set; }
    }
}
