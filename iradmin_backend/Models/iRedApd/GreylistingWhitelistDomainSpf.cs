﻿using System;
using System.Collections.Generic;

namespace iradmin_backend.Models
{
    public partial class GreylistingWhitelistDomainSpf
    {
        public long Id { get; set; }
        public string Account { get; set; }
        public string Sender { get; set; }
        public string Comment { get; set; }
    }
}
