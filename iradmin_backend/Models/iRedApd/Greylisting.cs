﻿using System;
using System.Collections.Generic;

namespace iradmin_backend.Models
{
    public partial class Greylisting
    {
        public long Id { get; set; }
        public string Account { get; set; }
        public byte Priority { get; set; }
        public string Sender { get; set; }
        public byte SenderPriority { get; set; }
        public string Comment { get; set; }
        public byte Active { get; set; }
    }
}
