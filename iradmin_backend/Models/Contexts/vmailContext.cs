﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.Extensions.Configuration;

using iradmin_backend.Models;

namespace iradmin_backend.Contexts
{
    public partial class vmailContext : DbContext
    {
        public vmailContext(DbContextOptions<vmailContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Admin> Admin { get; set; }
        public virtual DbSet<Alias> Alias { get; set; }
        public virtual DbSet<AliasDomain> AliasDomain { get; set; }
        public virtual DbSet<AnyoneShares> AnyoneShares { get; set; }
        public virtual DbSet<DeletedMailboxes> DeletedMailboxes { get; set; }
        public virtual DbSet<Domain> Domain { get; set; }
        public virtual DbSet<DomainAdmins> DomainAdmins { get; set; }
        public virtual DbSet<Forwardings> Forwardings { get; set; }
        public virtual DbSet<Mailbox> Mailbox { get; set; }
        public virtual DbSet<Maillists> Maillists { get; set; }
        public virtual DbSet<Moderators> Moderators { get; set; }
        public virtual DbSet<RecipientBccDomain> RecipientBccDomain { get; set; }
        public virtual DbSet<RecipientBccUser> RecipientBccUser { get; set; }
        public virtual DbSet<SenderBccDomain> SenderBccDomain { get; set; }
        public virtual DbSet<SenderBccUser> SenderBccUser { get; set; }
        public virtual DbSet<SenderRelayhost> SenderRelayhost { get; set; }
        public virtual DbSet<ShareFolder> ShareFolder { get; set; }
        public virtual DbSet<UsedQuota> UsedQuota { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Admin>(entity =>
            {
                entity.HasKey(e => e.Username);

                entity.ToTable("admin", "vmail");

                entity.HasIndex(e => e.Active)
                    .HasName("active");

                entity.HasIndex(e => e.Expired)
                    .HasName("expired");

                entity.HasIndex(e => e.Passwordlastchange)
                    .HasName("passwordlastchange");

                entity.Property(e => e.Username)
                    .HasColumnName("username")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("''");

                entity.Property(e => e.Active)
                    .HasColumnName("active")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.Created)
                    .HasColumnName("created")
                    .HasDefaultValueSql("'1970-01-01 01:01:01'");

                entity.Property(e => e.Expired)
                    .HasColumnName("expired")
                    .HasDefaultValueSql("'9999-12-31 00:00:00'");

                entity.Property(e => e.Language)
                    .IsRequired()
                    .HasColumnName("language")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("''");

                entity.Property(e => e.Modified)
                    .HasColumnName("modified")
                    .HasDefaultValueSql("'1970-01-01 01:01:01'");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("''");

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasColumnName("password")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("''");

                entity.Property(e => e.Passwordlastchange)
                    .HasColumnName("passwordlastchange")
                    .HasDefaultValueSql("'1970-01-01 01:01:01'");

                entity.Property(e => e.Settings)
                    .HasColumnName("settings")
                    .IsUnicode(false)
                    .HasDefaultValueSql("NULL");
            });

            modelBuilder.Entity<Alias>(entity =>
            {
                entity.HasKey(e => e.Address);

                entity.ToTable("alias", "vmail");

                entity.HasIndex(e => e.Active)
                    .HasName("active");

                entity.HasIndex(e => e.Domain)
                    .HasName("domain");

                entity.HasIndex(e => e.Expired)
                    .HasName("expired");

                entity.Property(e => e.Address)
                    .HasColumnName("address")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("''");

                entity.Property(e => e.Accesspolicy)
                    .IsRequired()
                    .HasColumnName("accesspolicy")
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasDefaultValueSql("''");

                entity.Property(e => e.Active)
                    .HasColumnName("active")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.Created)
                    .HasColumnName("created")
                    .HasDefaultValueSql("'1970-01-01 01:01:01'");

                entity.Property(e => e.Domain)
                    .IsRequired()
                    .HasColumnName("domain")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("''");

                entity.Property(e => e.Expired)
                    .HasColumnName("expired")
                    .HasDefaultValueSql("'9999-12-31 00:00:00'");

                entity.Property(e => e.Modified)
                    .HasColumnName("modified")
                    .HasDefaultValueSql("'1970-01-01 01:01:01'");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("''");
            });

            modelBuilder.Entity<AliasDomain>(entity =>
            {
                entity.HasKey(e => e.AliasDomain1);

                entity.ToTable("alias_domain", "vmail");

                entity.HasIndex(e => e.Active)
                    .HasName("active");

                entity.HasIndex(e => e.TargetDomain)
                    .HasName("target_domain");

                entity.Property(e => e.AliasDomain1)
                    .HasColumnName("alias_domain")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.Active)
                    .HasColumnName("active")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.Created)
                    .HasColumnName("created")
                    .HasDefaultValueSql("'1970-01-01 01:01:01'");

                entity.Property(e => e.Modified)
                    .HasColumnName("modified")
                    .HasDefaultValueSql("'1970-01-01 01:01:01'");

                entity.Property(e => e.TargetDomain)
                    .IsRequired()
                    .HasColumnName("target_domain")
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<AnyoneShares>(entity =>
            {
                entity.HasKey(e => e.FromUser);

                entity.ToTable("anyone_shares", "vmail");

                entity.Property(e => e.FromUser)
                    .HasColumnName("from_user")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.Dummy)
                    .HasColumnName("dummy")
                    .HasColumnType("char(1)")
                    .HasDefaultValueSql("'1'");
            });

            modelBuilder.Entity<DeletedMailboxes>(entity => {
                entity.ToTable("deleted_mailboxes", "vmail");

                entity.HasIndex(e => e.Id)
                    .HasName("id");

                entity.HasIndex(e => e.Timestamp)
                    .HasName("timestamp");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Timestamp)
                    .HasColumnName("timestamp")
                    .HasDefaultValueSql("getdate()");

                entity.Property(e => e.Username)
                    .HasColumnName("username")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("''");

                entity.Property(e => e.Domain)
                    .HasColumnName("domain")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("''");

                entity.Property(e => e.Maildir)
                    .HasColumnName("maildir")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("''");

                entity.Property(e => e.Admin)
                    .HasColumnName("admin")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("''");

                entity.Property(e => e.DeleteDate)
                    .HasColumnName("delete_date");
            });

            modelBuilder.Entity<Domain>(entity =>
            {
                entity.HasKey(e => e.Domainname);

                entity.ToTable("domain", "vmail");

                entity.HasIndex(e => e.Active)
                    .HasName("active");

                entity.HasIndex(e => e.Backupmx)
                    .HasName("backupmx");

                entity.HasIndex(e => e.Expired)
                    .HasName("expired");

                entity.Property(e => e.Domainname)
                    .HasColumnName("domain")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("''");

                entity.Property(e => e.Active)
                    .HasColumnName("active")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.Aliases)
                    .HasColumnName("aliases")
                    .HasColumnType("int(10)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Backupmx)
                    .HasColumnName("backupmx")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Created)
                    .HasColumnName("created")
                    .HasDefaultValueSql("'1970-01-01 01:01:01'");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .IsUnicode(false)
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.Disclaimer)
                    .HasColumnName("disclaimer")
                    .IsUnicode(false)
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.Expired)
                    .HasColumnName("expired")
                    .HasDefaultValueSql("'9999-12-31 00:00:00'");

                entity.Property(e => e.Mailboxes)
                    .HasColumnName("mailboxes")
                    .HasColumnType("int(10)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Maillists)
                    .HasColumnName("maillists")
                    .HasColumnType("int(10)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Maxquota)
                    .HasColumnName("maxquota")
                    .HasColumnType("bigint(20)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Modified)
                    .HasColumnName("modified")
                    .HasDefaultValueSql("'1970-01-01 01:01:01'");

                entity.Property(e => e.Quota)
                    .HasColumnName("quota")
                    .HasColumnType("bigint(20)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Settings)
                    .HasColumnName("settings")
                    .IsUnicode(false)
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.Transport)
                    .IsRequired()
                    .HasColumnName("transport")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("'dovecot'");
            });

            modelBuilder.Entity<DomainAdmins>(entity =>
            {
                entity.HasKey(e => new { e.Username, e.Domain });

                entity.ToTable("domain_admins", "vmail");

                entity.HasIndex(e => e.Active)
                    .HasName("active");

                entity.HasIndex(e => e.Domain)
                    .HasName("domain");

                entity.HasIndex(e => e.Username)
                    .HasName("username");

                entity.Property(e => e.Username)
                    .HasColumnName("username")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("''");

                entity.Property(e => e.Domain)
                    .HasColumnName("domain")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("''");

                entity.Property(e => e.Active)
                    .HasColumnName("active")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.Created)
                    .HasColumnName("created")
                    .HasDefaultValueSql("'1970-01-01 01:01:01'");

                entity.Property(e => e.Expired)
                    .HasColumnName("expired")
                    .HasDefaultValueSql("'9999-12-31 00:00:00'");

                entity.Property(e => e.Modified)
                    .HasColumnName("modified")
                    .HasDefaultValueSql("'1970-01-01 01:01:01'");
            });

            modelBuilder.Entity<Forwardings>(entity =>
            {
                entity.ToTable("forwardings", "vmail");

                entity.HasIndex(e => e.DestDomain)
                    .HasName("dest_domain");

                entity.HasIndex(e => e.Domain)
                    .HasName("domain");

                entity.HasIndex(e => e.IsAlias)
                    .HasName("is_alias");

                entity.HasIndex(e => e.IsList)
                    .HasName("is_list");

                entity.HasIndex(e => e.IsMaillist)
                    .HasName("is_maillist");

                entity.HasIndex(e => new { e.Address, e.Forwarding })
                    .HasName("address")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20) unsigned");

                entity.Property(e => e.Active)
                    .HasColumnName("active")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.Address)
                    .IsRequired()
                    .HasColumnName("address")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("''");

                entity.Property(e => e.DestDomain)
                    .IsRequired()
                    .HasColumnName("dest_domain")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("''");

                entity.Property(e => e.Domain)
                    .IsRequired()
                    .HasColumnName("domain")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("''");

                entity.Property(e => e.Forwarding)
                    .IsRequired()
                    .HasColumnName("forwarding")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("''");

                entity.Property(e => e.IsAlias)
                    .HasColumnName("is_alias")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.IsForwarding)
                    .HasColumnName("is_forwarding")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.IsList)
                    .HasColumnName("is_list")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.IsMaillist)
                    .HasColumnName("is_maillist")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("0");
            });

            modelBuilder.Entity<Mailbox>(entity =>
            {
                entity.HasKey(e => e.Username);

                entity.ToTable("mailbox", "vmail");

                entity.HasIndex(e => e.Active)
                    .HasName("active");

                entity.HasIndex(e => e.Department)
                    .HasName("department");

                entity.HasIndex(e => e.Domain)
                    .HasName("domain");

                entity.HasIndex(e => e.Employeeid)
                    .HasName("employeeid");

                entity.HasIndex(e => e.Enabledeliver)
                    .HasName("enabledeliver");

                entity.HasIndex(e => e.Enabledoveadm)
                    .HasName("enabledoveadm");

                entity.HasIndex(e => e.Enabledsync)
                    .HasName("enabledsync");

                entity.HasIndex(e => e.Enableimap)
                    .HasName("enableimap");

                entity.HasIndex(e => e.Enableimapsecured)
                    .HasName("enableimapsecured");

                entity.HasIndex(e => e.Enableimaptls)
                    .HasName("enableimaptls");

                entity.HasIndex(e => e.EnableindexerWorker)
                    .HasName("enableindexer-worker");

                entity.HasIndex(e => e.Enableinternal)
                    .HasName("enableinternal");

                entity.HasIndex(e => e.Enablelda)
                    .HasName("enablelda");

                entity.HasIndex(e => e.EnablelibStorage)
                    .HasName("enablelib-storage");

                entity.HasIndex(e => e.Enablelmtp)
                    .HasName("enablelmtp");

                entity.HasIndex(e => e.Enablemanagesieve)
                    .HasName("enablemanagesieve");

                entity.HasIndex(e => e.Enablemanagesievesecured)
                    .HasName("enablemanagesievesecured");

                entity.HasIndex(e => e.Enablepop3)
                    .HasName("enablepop3");

                entity.HasIndex(e => e.Enablepop3secured)
                    .HasName("enablepop3secured");

                entity.HasIndex(e => e.Enablepop3tls)
                    .HasName("enablepop3tls");

                entity.HasIndex(e => e.Enablesieve)
                    .HasName("enablesieve");

                entity.HasIndex(e => e.Enablesievesecured)
                    .HasName("enablesievesecured");

                entity.HasIndex(e => e.Enablesievetls)
                    .HasName("enablesievetls");

                entity.HasIndex(e => e.Enablesmtp)
                    .HasName("enablesmtp");

                entity.HasIndex(e => e.Enablesmtpsecured)
                    .HasName("enablesmtpsecured");

                entity.HasIndex(e => e.Enablesogo)
                    .HasName("enablesogo");

                entity.HasIndex(e => e.Expired)
                    .HasName("expired");

                entity.HasIndex(e => e.Isadmin)
                    .HasName("isadmin");

                entity.HasIndex(e => e.Isglobaladmin)
                    .HasName("isglobaladmin");

                entity.HasIndex(e => e.Passwordlastchange)
                    .HasName("passwordlastchange");

                entity.Property(e => e.Username)
                    .HasColumnName("username")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("''");

                entity.Property(e => e.Active)
                    .HasColumnName("active")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.AllowNets)
                    .HasColumnName("allow_nets")
                    .IsUnicode(false)
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.Allowedrecipients)
                    .HasColumnName("allowedrecipients")
                    .IsUnicode(false)
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.Allowedsenders)
                    .HasColumnName("allowedsenders")
                    .IsUnicode(false)
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.Created)
                    .HasColumnName("created")
                    .HasDefaultValueSql("'1970-01-01 01:01:01'");

                entity.Property(e => e.Department)
                    .IsRequired()
                    .HasColumnName("department")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("''");

                entity.Property(e => e.Disclaimer)
                    .HasColumnName("disclaimer")
                    .IsUnicode(false)
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.Domain)
                    .IsRequired()
                    .HasColumnName("domain")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("''");

                entity.Property(e => e.Employeeid)
                    .HasColumnName("employeeid")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("''");

                entity.Property(e => e.Enabledeliver)
                    .HasColumnName("enabledeliver")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.Enabledoveadm)
                    .HasColumnName("enabledoveadm")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.Enabledsync)
                    .HasColumnName("enabledsync")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.Enableimap)
                    .HasColumnName("enableimap")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.Enableimapsecured)
                    .HasColumnName("enableimapsecured")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.Enableimaptls)
                    .HasColumnName("enableimaptls")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.EnableindexerWorker)
                    .HasColumnName("enableindexer-worker")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.Enableinternal)
                    .HasColumnName("enableinternal")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.Enablelda)
                    .HasColumnName("enablelda")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.EnablelibStorage)
                    .HasColumnName("enablelib-storage")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.Enablelmtp)
                    .HasColumnName("enablelmtp")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.Enablemanagesieve)
                    .HasColumnName("enablemanagesieve")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.Enablemanagesievesecured)
                    .HasColumnName("enablemanagesievesecured")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.Enablepop3)
                    .HasColumnName("enablepop3")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.Enablepop3secured)
                    .HasColumnName("enablepop3secured")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.Enablepop3tls)
                    .HasColumnName("enablepop3tls")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.Enablesieve)
                    .HasColumnName("enablesieve")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.Enablesievesecured)
                    .HasColumnName("enablesievesecured")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.Enablesievetls)
                    .HasColumnName("enablesievetls")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.Enablesmtp)
                    .HasColumnName("enablesmtp")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.Enablesmtpsecured)
                    .HasColumnName("enablesmtpsecured")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.Enablesogo)
                    .HasColumnName("enablesogo")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.Expired)
                    .HasColumnName("expired")
                    .HasDefaultValueSql("'9999-12-31 00:00:00'");

                entity.Property(e => e.Isadmin)
                    .HasColumnName("isadmin")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Isglobaladmin)
                    .HasColumnName("isglobaladmin")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Language)
                    .IsRequired()
                    .HasColumnName("language")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("''");

                entity.Property(e => e.Lastlogindate)
                    .HasColumnName("lastlogindate")
                    .HasDefaultValueSql("'1970-01-01 01:01:01'");

                entity.Property(e => e.Lastloginipv4)
                    .HasColumnName("lastloginipv4")
                    .HasColumnType("int(4) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Lastloginprotocol)
                    .IsRequired()
                    .HasColumnName("lastloginprotocol")
                    .HasColumnType("char(255)")
                    .HasDefaultValueSql("''");

                entity.Property(e => e.Maildir)
                    .IsRequired()
                    .HasColumnName("maildir")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("''");

                entity.Property(e => e.Modified)
                    .HasColumnName("modified")
                    .HasDefaultValueSql("'1970-01-01 01:01:01'");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("''");

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasColumnName("password")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("''");

                entity.Property(e => e.Passwordlastchange)
                    .HasColumnName("passwordlastchange")
                    .HasDefaultValueSql("'1970-01-01 01:01:01'");

                entity.Property(e => e.Quota)
                    .HasColumnName("quota")
                    .HasColumnType("bigint(20)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Rank)
                    .IsRequired()
                    .HasColumnName("rank")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("'normal'");

                entity.Property(e => e.Rejectedrecipients)
                    .HasColumnName("rejectedrecipients")
                    .IsUnicode(false)
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.Rejectedsenders)
                    .HasColumnName("rejectedsenders")
                    .IsUnicode(false)
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.Settings)
                    .HasColumnName("settings")
                    .IsUnicode(false)
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.Storagebasedirectory)
                    .IsRequired()
                    .HasColumnName("storagebasedirectory")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("'/mnt/mail'");

                entity.Property(e => e.Storagenode)
                    .IsRequired()
                    .HasColumnName("storagenode")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("'vmail1'");

                entity.Property(e => e.Transport)
                    .IsRequired()
                    .HasColumnName("transport")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("''");
            });

            modelBuilder.Entity<Maillists>(entity =>
            {
                entity.ToTable("maillists", "vmail");

                entity.HasIndex(e => e.Active)
                    .HasName("active");

                entity.HasIndex(e => e.Address)
                    .HasName("address")
                    .IsUnique();

                entity.HasIndex(e => e.Domain)
                    .HasName("domain");

                entity.HasIndex(e => e.IsNewsletter)
                    .HasName("is_newsletter");

                entity.HasIndex(e => e.Mlid)
                    .HasName("mlid")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20) unsigned");

                entity.Property(e => e.Accesspolicy)
                    .IsRequired()
                    .HasColumnName("accesspolicy")
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasDefaultValueSql("''");

                entity.Property(e => e.Active)
                    .HasColumnName("active")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.Address)
                    .IsRequired()
                    .HasColumnName("address")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("''");

                entity.Property(e => e.Created)
                    .HasColumnName("created")
                    .HasDefaultValueSql("'1970-01-01 01:01:01'");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .IsUnicode(false)
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.Domain)
                    .IsRequired()
                    .HasColumnName("domain")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("''");

                entity.Property(e => e.Expired)
                    .HasColumnName("expired")
                    .HasDefaultValueSql("'9999-12-31 00:00:00'");

                entity.Property(e => e.IsNewsletter)
                    .HasColumnName("is_newsletter")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Maxmsgsize)
                    .HasColumnName("maxmsgsize")
                    .HasColumnType("bigint(20)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Mlid)
                    .IsRequired()
                    .HasColumnName("mlid")
                    .HasMaxLength(36)
                    .IsUnicode(false)
                    .HasDefaultValueSql("''");

                entity.Property(e => e.Modified)
                    .HasColumnName("modified")
                    .HasDefaultValueSql("'1970-01-01 01:01:01'");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("''");

                entity.Property(e => e.Settings)
                    .HasColumnName("settings")
                    .IsUnicode(false)
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.Transport)
                    .IsRequired()
                    .HasColumnName("transport")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("''");
            });

            modelBuilder.Entity<Moderators>(entity =>
            {
                entity.ToTable("moderators", "vmail");

                entity.HasIndex(e => e.DestDomain)
                    .HasName("dest_domain");

                entity.HasIndex(e => e.Domain)
                    .HasName("domain");

                entity.HasIndex(e => new { e.Address, e.Moderator })
                    .HasName("address")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20) unsigned");

                entity.Property(e => e.Address)
                    .IsRequired()
                    .HasColumnName("address")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("''");

                entity.Property(e => e.DestDomain)
                    .IsRequired()
                    .HasColumnName("dest_domain")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("''");

                entity.Property(e => e.Domain)
                    .IsRequired()
                    .HasColumnName("domain")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("''");

                entity.Property(e => e.Moderator)
                    .IsRequired()
                    .HasColumnName("moderator")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("''");
            });

            modelBuilder.Entity<RecipientBccDomain>(entity =>
            {
                entity.HasKey(e => e.Domain);

                entity.ToTable("recipient_bcc_domain", "vmail");

                entity.HasIndex(e => e.Active)
                    .HasName("active");

                entity.HasIndex(e => e.BccAddress)
                    .HasName("bcc_address");

                entity.HasIndex(e => e.Expired)
                    .HasName("expired");

                entity.Property(e => e.Domain)
                    .HasColumnName("domain")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("''");

                entity.Property(e => e.Active)
                    .HasColumnName("active")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.BccAddress)
                    .IsRequired()
                    .HasColumnName("bcc_address")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("''");

                entity.Property(e => e.Created)
                    .HasColumnName("created")
                    .HasDefaultValueSql("'1970-01-01 01:01:01'");

                entity.Property(e => e.Expired)
                    .HasColumnName("expired")
                    .HasDefaultValueSql("'9999-12-31 00:00:00'");

                entity.Property(e => e.Modified)
                    .HasColumnName("modified")
                    .HasDefaultValueSql("'1970-01-01 01:01:01'");
            });

            modelBuilder.Entity<RecipientBccUser>(entity =>
            {
                entity.HasKey(e => e.Username);

                entity.ToTable("recipient_bcc_user", "vmail");

                entity.HasIndex(e => e.Active)
                    .HasName("active");

                entity.HasIndex(e => e.BccAddress)
                    .HasName("bcc_address");

                entity.HasIndex(e => e.Expired)
                    .HasName("expired");

                entity.Property(e => e.Username)
                    .HasColumnName("username")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("''");

                entity.Property(e => e.Active)
                    .HasColumnName("active")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.BccAddress)
                    .IsRequired()
                    .HasColumnName("bcc_address")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("''");

                entity.Property(e => e.Created)
                    .HasColumnName("created")
                    .HasDefaultValueSql("'1970-01-01 01:01:01'");

                entity.Property(e => e.Domain)
                    .IsRequired()
                    .HasColumnName("domain")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("''");

                entity.Property(e => e.Expired)
                    .HasColumnName("expired")
                    .HasDefaultValueSql("'9999-12-31 00:00:00'");

                entity.Property(e => e.Modified)
                    .HasColumnName("modified")
                    .HasDefaultValueSql("'1970-01-01 01:01:01'");
            });

            modelBuilder.Entity<SenderBccDomain>(entity =>
            {
                entity.HasKey(e => e.Domain);

                entity.ToTable("sender_bcc_domain", "vmail");

                entity.HasIndex(e => e.Active)
                    .HasName("active");

                entity.HasIndex(e => e.BccAddress)
                    .HasName("bcc_address");

                entity.HasIndex(e => e.Expired)
                    .HasName("expired");

                entity.Property(e => e.Domain)
                    .HasColumnName("domain")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("''");

                entity.Property(e => e.Active)
                    .HasColumnName("active")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.BccAddress)
                    .IsRequired()
                    .HasColumnName("bcc_address")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("''");

                entity.Property(e => e.Created)
                    .HasColumnName("created")
                    .HasDefaultValueSql("'1970-01-01 01:01:01'");

                entity.Property(e => e.Expired)
                    .HasColumnName("expired")
                    .HasDefaultValueSql("'9999-12-31 00:00:00'");

                entity.Property(e => e.Modified)
                    .HasColumnName("modified")
                    .HasDefaultValueSql("'1970-01-01 01:01:01'");
            });

            modelBuilder.Entity<SenderBccUser>(entity =>
            {
                entity.HasKey(e => e.Username);

                entity.ToTable("sender_bcc_user", "vmail");

                entity.HasIndex(e => e.Active)
                    .HasName("active");

                entity.HasIndex(e => e.BccAddress)
                    .HasName("bcc_address");

                entity.HasIndex(e => e.Domain)
                    .HasName("domain");

                entity.HasIndex(e => e.Expired)
                    .HasName("expired");

                entity.Property(e => e.Username)
                    .HasColumnName("username")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("''");

                entity.Property(e => e.Active)
                    .HasColumnName("active")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.BccAddress)
                    .IsRequired()
                    .HasColumnName("bcc_address")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("''");

                entity.Property(e => e.Created)
                    .HasColumnName("created")
                    .HasDefaultValueSql("'1970-01-01 01:01:01'");

                entity.Property(e => e.Domain)
                    .IsRequired()
                    .HasColumnName("domain")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("''");

                entity.Property(e => e.Expired)
                    .HasColumnName("expired")
                    .HasDefaultValueSql("'9999-12-31 00:00:00'");

                entity.Property(e => e.Modified)
                    .HasColumnName("modified")
                    .HasDefaultValueSql("'1970-01-01 01:01:01'");
            });

            modelBuilder.Entity<SenderRelayhost>(entity =>
            {
                entity.ToTable("sender_relayhost", "vmail");

                entity.HasIndex(e => e.Account)
                    .HasName("account")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20) unsigned");

                entity.Property(e => e.Account)
                    .IsRequired()
                    .HasColumnName("account")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("''");

                entity.Property(e => e.Relayhost)
                    .IsRequired()
                    .HasColumnName("relayhost")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("''");
            });

            modelBuilder.Entity<ShareFolder>(entity =>
            {
                entity.HasKey(e => new { e.FromUser, e.ToUser });

                entity.ToTable("share_folder", "vmail");

                entity.HasIndex(e => e.FromUser)
                    .HasName("from_user");

                entity.HasIndex(e => e.ToUser)
                    .HasName("to_user");

                entity.Property(e => e.FromUser)
                    .HasColumnName("from_user")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ToUser)
                    .HasColumnName("to_user")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Dummy)
                    .HasColumnName("dummy")
                    .HasColumnType("char(1)")
                    .HasDefaultValueSql("NULL");
            });

            modelBuilder.Entity<UsedQuota>(entity =>
            {
                entity.HasKey(e => e.Username);

                entity.ToTable("used_quota", "vmail");

                entity.HasIndex(e => e.Domain)
                    .HasName("domain");

                entity.Property(e => e.Username)
                    .HasColumnName("username")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.Bytes)
                    .HasColumnName("bytes")
                    .HasColumnType("bigint(20)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Domain)
                    .IsRequired()
                    .HasColumnName("domain")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("''");

                entity.Property(e => e.Messages)
                    .HasColumnName("messages")
                    .HasColumnType("bigint(20)")
                    .HasDefaultValueSql("0");
            });
        }
    }
}
