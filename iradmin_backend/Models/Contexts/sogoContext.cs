﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.Extensions.Configuration;

using iradmin_backend.Models;

namespace iradmin_backend.Contexts
{
    public partial class sogoContext : DbContext
    {
        public sogoContext(DbContextOptions<sogoContext> options)
            : base(options)
        {
        }

        public virtual DbSet<SogoAcl> SogoAcl { get; set; }
        public virtual DbSet<SogoAlarmsFolder> SogoAlarmsFolder { get; set; }
        public virtual DbSet<SogoCacheFolder> SogoCacheFolder { get; set; }
        public virtual DbSet<SogoFolderInfo> SogoFolderInfo { get; set; }
        public virtual DbSet<SogoQuickAppointment> SogoQuickAppointment { get; set; }
        public virtual DbSet<SogoQuickContact> SogoQuickContact { get; set; }
        public virtual DbSet<SogoSessionsFolder> SogoSessionsFolder { get; set; }
        public virtual DbSet<SogoStore> SogoStore { get; set; }
        public virtual DbSet<SogoUserProfile> SogoUserProfile { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<SogoAcl>(entity =>
            {
                entity.ToTable("sogo_acl", "sogo");


                entity.Property(e => e.CFolderId)
                    .HasColumnName("c_folder_id")
                    .HasColumnType("int(11)");
                
                entity.Property(e => e.CObject)
                    .HasColumnName("c_object")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.CUid)
                    .HasColumnName("c_uid")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.CRole)
                    .HasColumnName("c_role")
                    .HasMaxLength(80)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SogoAlarmsFolder>(entity =>
            {
                entity.ToTable("sogo_alarms_folder", "sogo");

                entity.Property(e => e.CPath)
                    .HasColumnName("c_path")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.CName)
                    .HasColumnName("c_name")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.CUid)
                    .HasColumnName("c_uid")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.CRecurrenceId)
                    .HasColumnName("c_recurrence_id")
                    .HasColumnType("int(11)");
                
                entity.Property(e => e.CAlarmNumber)
                    .HasColumnName("c_alarm_number")
                    .HasColumnType("int(11)");

                entity.Property(e => e.CAlarmDate)
                    .HasColumnName("c_alarm_date")
                    .HasColumnType("int(11)");

            });

            modelBuilder.Entity<SogoCacheFolder>(entity =>
            {
                entity.HasKey(e => new { e.CUid, e.CPath });

                entity.ToTable("sogo_cache_folder", "sogo");

                entity.Property(e => e.CUid)
                    .HasColumnName("c_uid")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.CPath)
                    .HasColumnName("c_path")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.CContent)
                    .HasColumnName("c_content")
                    .HasColumnType("longtext")
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.CCreationdate)
                    .HasColumnName("c_creationdate")
                    .HasColumnType("int(11)");

                entity.Property(e => e.CDeleted)
                    .HasColumnName("c_deleted")
                    .HasColumnType("tinyint(4)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.CLastmodified)
                    .HasColumnName("c_lastmodified")
                    .HasColumnType("int(11)");

                entity.Property(e => e.CParentPath)
                    .HasColumnName("c_parent_path")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.CType)
                    .HasColumnName("c_type")
                    .HasColumnType("tinyint(3) unsigned");

                entity.Property(e => e.CVersion)
                    .HasColumnName("c_version")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("0");
            });

            modelBuilder.Entity<SogoFolderInfo>(entity =>
            {
                entity.HasKey(e => e.CPath);

                entity.ToTable("sogo_folder_info", "sogo");

                entity.HasIndex(e => e.CFolderId)
                    .HasName("c_folder_id")
                    .IsUnique();

                entity.Property(e => e.CPath)
                    .HasColumnName("c_path")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CAclLocation)
                    .HasColumnName("c_acl_location")
                    .HasMaxLength(2048)
                    .IsUnicode(false)
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.CFolderId)
                    .HasColumnName("c_folder_id")
                    .HasColumnType("bigint(20) unsigned");

                entity.Property(e => e.CFolderType)
                    .IsRequired()
                    .HasColumnName("c_folder_type")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.CFoldername)
                    .IsRequired()
                    .HasColumnName("c_foldername")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.CLocation)
                    .HasColumnName("c_location")
                    .HasMaxLength(2048)
                    .IsUnicode(false)
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.CPath1)
                    .IsRequired()
                    .HasColumnName("c_path1")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.CPath2)
                    .HasColumnName("c_path2")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.CPath3)
                    .HasColumnName("c_path3")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.CPath4)
                    .HasColumnName("c_path4")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.CQuickLocation)
                    .HasColumnName("c_quick_location")
                    .HasMaxLength(2048)
                    .IsUnicode(false)
                    .HasDefaultValueSql("NULL");
            });

            modelBuilder.Entity<SogoQuickAppointment>(entity =>
            {
                entity.HasKey(e => new { e.CFolderId, e.CName });

                entity.ToTable("sogo_quick_appointment", "sogo");

                entity.Property(e => e.CFolderId)
                    .HasColumnName("c_folder_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.CName)
                    .HasColumnName("c_name")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.CCategory)
                    .HasColumnName("c_category")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.CClassification)
                    .HasColumnName("c_classification")
                    .HasColumnType("int(11)");

                entity.Property(e => e.CComponent)
                    .IsRequired()
                    .HasColumnName("c_component")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.CCycleenddate)
                    .HasColumnName("c_cycleenddate")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.CCycleinfo)
                    .HasColumnName("c_cycleinfo")
                    .IsUnicode(false)
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.CDescription)
                    .HasColumnName("c_description")
                    .IsUnicode(false)
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.CEnddate)
                    .HasColumnName("c_enddate")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.CIsallday)
                    .HasColumnName("c_isallday")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.CIscycle)
                    .HasColumnName("c_iscycle")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.CIsopaque)
                    .HasColumnName("c_isopaque")
                    .HasColumnType("int(11)");

                entity.Property(e => e.CLocation)
                    .HasColumnName("c_location")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.CNextalarm)
                    .HasColumnName("c_nextalarm")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.COrgmail)
                    .HasColumnName("c_orgmail")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.CParticipants)
                    .HasColumnName("c_participants")
                    .IsUnicode(false)
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.CPartmails)
                    .HasColumnName("c_partmails")
                    .IsUnicode(false)
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.CPartstates)
                    .HasColumnName("c_partstates")
                    .IsUnicode(false)
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.CPriority)
                    .HasColumnName("c_priority")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.CSequence)
                    .HasColumnName("c_sequence")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.CStartdate)
                    .HasColumnName("c_startdate")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.CStatus)
                    .HasColumnName("c_status")
                    .HasColumnType("int(11)");

                entity.Property(e => e.CTitle)
                    .IsRequired()
                    .HasColumnName("c_title")
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.CUid)
                    .IsRequired()
                    .HasColumnName("c_uid")
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SogoQuickContact>(entity =>
            {
                entity.HasKey(e => new { e.CFolderId, e.CName });

                entity.ToTable("sogo_quick_contact", "sogo");

                entity.Property(e => e.CFolderId)
                    .HasColumnName("c_folder_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.CName)
                    .HasColumnName("c_name")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.CCategories)
                    .HasColumnName("c_categories")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.CCn)
                    .HasColumnName("c_cn")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.CComponent)
                    .IsRequired()
                    .HasColumnName("c_component")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.CGivenname)
                    .HasColumnName("c_givenname")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.CHascertificate)
                    .HasColumnName("c_hascertificate")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.CL)
                    .HasColumnName("c_l")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.CMail)
                    .HasColumnName("c_mail")
                    .IsUnicode(false)
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.CO)
                    .HasColumnName("c_o")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.COu)
                    .HasColumnName("c_ou")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.CScreenname)
                    .HasColumnName("c_screenname")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.CSn)
                    .HasColumnName("c_sn")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.CTelephonenumber)
                    .HasColumnName("c_telephonenumber")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("NULL");
            });

            modelBuilder.Entity<SogoSessionsFolder>(entity =>
            {
                entity.HasKey(e => e.CId);

                entity.ToTable("sogo_sessions_folder", "sogo");

                entity.Property(e => e.CId)
                    .HasColumnName("c_id")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CCreationdate)
                    .HasColumnName("c_creationdate")
                    .HasColumnType("int(11)");

                entity.Property(e => e.CLastseen)
                    .HasColumnName("c_lastseen")
                    .HasColumnType("int(11)");

                entity.Property(e => e.CValue)
                    .IsRequired()
                    .HasColumnName("c_value")
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SogoStore>(entity =>
            {
                entity.HasKey(e => new { e.CFolderId, e.CName });

                entity.ToTable("sogo_store", "sogo");

                entity.Property(e => e.CFolderId)
                    .HasColumnName("c_folder_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.CName)
                    .HasColumnName("c_name")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.CContent)
                    .IsRequired()
                    .HasColumnName("c_content")
                    .HasColumnType("mediumtext");

                entity.Property(e => e.CCreationdate)
                    .HasColumnName("c_creationdate")
                    .HasColumnType("int(11)");

                entity.Property(e => e.CDeleted)
                    .HasColumnName("c_deleted")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.CLastmodified)
                    .HasColumnName("c_lastmodified")
                    .HasColumnType("int(11)");

                entity.Property(e => e.CVersion)
                    .HasColumnName("c_version")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<SogoUserProfile>(entity =>
            {
                entity.HasKey(e => e.CUid);

                entity.ToTable("sogo_user_profile", "sogo");

                entity.Property(e => e.CUid)
                    .HasColumnName("c_uid")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CDefaults)
                    .HasColumnName("c_defaults")
                    .IsUnicode(false)
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.CSettings)
                    .HasColumnName("c_settings")
                    .IsUnicode(false)
                    .HasDefaultValueSql("NULL");
            });
        }
    }
}
