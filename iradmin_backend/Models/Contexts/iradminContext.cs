﻿using System;
using Microsoft.EntityFrameworkCore;

using iradmin_backend.Models;

namespace iradmin_backend.Contexts
{
    public class iradminContext : DbContext
    {
        public iradminContext(DbContextOptions<iradminContext> options)
            : base(options) {}
    }
}
