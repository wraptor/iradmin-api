﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.Extensions.Configuration;

using iradmin_backend.Models;

namespace iradmin_backend.Contexts
{
    public partial class amavisdContext : DbContext
    {
        public amavisdContext(DbContextOptions<amavisdContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Maddr> Maddr { get; set; }
        public virtual DbSet<Mailaddr> Mailaddr { get; set; }
        public virtual DbSet<Msgrcpt> Msgrcpt { get; set; }
        public virtual DbSet<Msgs> Msgs { get; set; }
        public virtual DbSet<OutboundWblist> OutboundWblist { get; set; }
        public virtual DbSet<Policy> Policy { get; set; }
        public virtual DbSet<Quarantine> Quarantine { get; set; }
        public virtual DbSet<Users> Users { get; set; }
        public virtual DbSet<Wblist> Wblist { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Maddr>(entity =>
            {
                entity.ToTable("maddr", "amavisd");

                entity.HasIndex(e => e.Domain)
                    .HasName("maddr_idx_domain");

                entity.HasIndex(e => e.Email)
                    .HasName("maddr_idx_email");

                entity.HasIndex(e => e.EmailRaw)
                    .HasName("maddr_idx_email_raw");

                entity.HasIndex(e => new { e.PartitionTag, e.Email })
                    .HasName("part_email")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20) unsigned");

                entity.Property(e => e.Domain)
                    .IsRequired()
                    .HasColumnName("domain")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasColumnName("email")
                    .HasColumnType("varbinary(255)");

                entity.Property(e => e.EmailRaw)
                    .IsRequired()
                    .HasColumnName("email_raw")
                    .HasColumnType("varbinary(255)")
                    .HasDefaultValueSql("''");

                entity.Property(e => e.PartitionTag)
                    .HasColumnName("partition_tag")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("0");
            });

            modelBuilder.Entity<Mailaddr>(entity =>
            {
                entity.ToTable("mailaddr", "amavisd");

                entity.HasIndex(e => e.Email)
                    .HasName("email")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasColumnName("email")
                    .HasColumnType("varbinary(255)");

                entity.Property(e => e.Priority)
                    .HasColumnName("priority")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("7");
            });

            modelBuilder.Entity<Msgrcpt>(entity =>
            {
                entity.HasKey(e => new { e.PartitionTag, e.MailId, e.Rseqnum });

                entity.ToTable("msgrcpt", "amavisd");

                entity.HasIndex(e => e.MailId)
                    .HasName("msgrcpt_idx_mail_id");

                entity.HasIndex(e => e.Rid)
                    .HasName("msgrcpt_idx_rid");

                entity.Property(e => e.PartitionTag)
                    .HasColumnName("partition_tag")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.MailId)
                    .HasColumnName("mail_id")
                    .HasColumnType("varbinary(16)");

                entity.Property(e => e.Rseqnum)
                    .HasColumnName("rseqnum")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Bl)
                    .HasColumnName("bl")
                    .HasColumnType("char(1)")
                    .HasDefaultValueSql("''");

                entity.Property(e => e.BspamLevel)
                    .HasColumnName("bspam_level")
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.Content)
                    .IsRequired()
                    .HasColumnName("content")
                    .HasColumnType("char(1)")
                    .HasDefaultValueSql("''");

                entity.Property(e => e.Ds)
                    .IsRequired()
                    .HasColumnName("ds")
                    .HasColumnType("char(1)");

                entity.Property(e => e.IsLocal)
                    .IsRequired()
                    .HasColumnName("is_local")
                    .HasColumnType("char(1)")
                    .HasDefaultValueSql("''");

                entity.Property(e => e.Rid)
                    .HasColumnName("rid")
                    .HasColumnType("bigint(20) unsigned");

                entity.Property(e => e.Rs)
                    .IsRequired()
                    .HasColumnName("rs")
                    .HasColumnType("char(1)");

                entity.Property(e => e.SmtpResp)
                    .HasColumnName("smtp_resp")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("''");

                entity.Property(e => e.Wl)
                    .HasColumnName("wl")
                    .HasColumnType("char(1)")
                    .HasDefaultValueSql("''");
            });

            modelBuilder.Entity<Msgs>(entity =>
            {
                entity.HasKey(e => new { e.PartitionTag, e.MailId });

                entity.ToTable("msgs", "amavisd");

                entity.HasIndex(e => e.Content)
                    .HasName("msgs_idx_content");

                entity.HasIndex(e => e.MailId)
                    .HasName("msgs_idx_mail_id");

                entity.HasIndex(e => e.MessageId)
                    .HasName("msgs_idx_mess_id");

                entity.HasIndex(e => e.QuarType)
                    .HasName("msgs_idx_quar_type");

                entity.HasIndex(e => e.Sid)
                    .HasName("msgs_idx_sid");

                entity.HasIndex(e => e.SpamLevel)
                    .HasName("msgs_idx_spam_level");

                entity.HasIndex(e => e.TimeNum)
                    .HasName("msgs_idx_time_num");

                entity.HasIndex(e => new { e.Content, e.TimeNum })
                    .HasName("msgs_idx_content_time_num");

                entity.Property(e => e.PartitionTag)
                    .HasColumnName("partition_tag")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.MailId)
                    .HasColumnName("mail_id")
                    .HasColumnType("varbinary(16)");

                entity.Property(e => e.AmId)
                    .IsRequired()
                    .HasColumnName("am_id")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ClientAddr)
                    .HasColumnName("client_addr")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("''");

                entity.Property(e => e.Content)
                    .HasColumnName("content")
                    .HasColumnType("char(1)")
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.DsnSent)
                    .HasColumnName("dsn_sent")
                    .HasColumnType("char(1)")
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.FromAddr)
                    .HasColumnName("from_addr")
                    .HasColumnType("varbinary(255)")
                    .HasDefaultValueSql("''");

                entity.Property(e => e.Host)
                    .IsRequired()
                    .HasColumnName("host")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.MessageId)
                    .HasColumnName("message_id")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("''");

                entity.Property(e => e.Originating)
                    .IsRequired()
                    .HasColumnName("originating")
                    .HasColumnType("char(1)")
                    .HasDefaultValueSql("''");

                entity.Property(e => e.Policy)
                    .HasColumnName("policy")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("''");

                entity.Property(e => e.QuarLoc)
                    .HasColumnName("quar_loc")
                    .HasColumnType("varbinary(255)")
                    .HasDefaultValueSql("''");

                entity.Property(e => e.QuarType)
                    .HasColumnName("quar_type")
                    .HasColumnType("char(1)")
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.SecretId)
                    .HasColumnName("secret_id")
                    .HasColumnType("varbinary(16)")
                    .HasDefaultValueSql("''");

                entity.Property(e => e.Sid)
                    .HasColumnName("sid")
                    .HasColumnType("bigint(20) unsigned");

                entity.Property(e => e.Size)
                    .HasColumnName("size")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.SpamLevel)
                    .HasColumnName("spam_level")
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.Subject)
                    .HasColumnName("subject")
                    .HasColumnType("varbinary(255)")
                    .HasDefaultValueSql("''");

                entity.Property(e => e.TimeIso)
                    .IsRequired()
                    .HasColumnName("time_iso")
                    .HasColumnType("char(16)");

                entity.Property(e => e.TimeNum)
                    .HasColumnName("time_num")
                    .HasColumnType("int(10) unsigned");
            });

            modelBuilder.Entity<OutboundWblist>(entity =>
            {
                entity.HasKey(e => new { e.Rid, e.Sid });

                entity.ToTable("outbound_wblist", "amavisd");

                entity.Property(e => e.Rid)
                    .HasColumnName("rid")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Sid)
                    .HasColumnName("sid")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Wb)
                    .IsRequired()
                    .HasColumnName("wb")
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Policy>(entity =>
            {
                entity.ToTable("policy", "amavisd");

                entity.HasIndex(e => e.PolicyName)
                    .HasName("policy_idx_policy_name")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.AddrExtensionBadHeader)
                    .HasColumnName("addr_extension_bad_header")
                    .HasMaxLength(64)
                    .IsUnicode(false)
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.AddrExtensionBanned)
                    .HasColumnName("addr_extension_banned")
                    .HasMaxLength(64)
                    .IsUnicode(false)
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.AddrExtensionSpam)
                    .HasColumnName("addr_extension_spam")
                    .HasMaxLength(64)
                    .IsUnicode(false)
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.AddrExtensionVirus)
                    .HasColumnName("addr_extension_virus")
                    .HasMaxLength(64)
                    .IsUnicode(false)
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.ArchiveQuarantineTo)
                    .HasColumnName("archive_quarantine_to")
                    .HasMaxLength(64)
                    .IsUnicode(false)
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.BadHeaderAdmin)
                    .HasColumnName("bad_header_admin")
                    .HasMaxLength(64)
                    .IsUnicode(false)
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.BadHeaderLover)
                    .HasColumnName("bad_header_lover")
                    .HasColumnType("char(1)")
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.BadHeaderQuarantineTo)
                    .HasColumnName("bad_header_quarantine_to")
                    .HasMaxLength(64)
                    .IsUnicode(false)
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.BannedAdmin)
                    .HasColumnName("banned_admin")
                    .HasMaxLength(64)
                    .IsUnicode(false)
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.BannedFilesLover)
                    .HasColumnName("banned_files_lover")
                    .HasColumnType("char(1)")
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.BannedQuarantineTo)
                    .HasColumnName("banned_quarantine_to")
                    .HasMaxLength(64)
                    .IsUnicode(false)
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.BannedRulenames)
                    .HasColumnName("banned_rulenames")
                    .HasMaxLength(64)
                    .IsUnicode(false)
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.BypassBannedChecks)
                    .HasColumnName("bypass_banned_checks")
                    .HasColumnType("char(1)")
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.BypassHeaderChecks)
                    .HasColumnName("bypass_header_checks")
                    .HasColumnType("char(1)")
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.BypassSpamChecks)
                    .HasColumnName("bypass_spam_checks")
                    .HasColumnType("char(1)")
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.BypassVirusChecks)
                    .HasColumnName("bypass_virus_checks")
                    .HasColumnType("char(1)")
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.CleanQuarantineTo)
                    .HasColumnName("clean_quarantine_to")
                    .HasMaxLength(64)
                    .IsUnicode(false)
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.DisclaimerOptions)
                    .HasColumnName("disclaimer_options")
                    .HasMaxLength(64)
                    .IsUnicode(false)
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.ForwardMethod)
                    .HasColumnName("forward_method")
                    .HasMaxLength(64)
                    .IsUnicode(false)
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.MessageSizeLimit)
                    .HasColumnName("message_size_limit")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.NewvirusAdmin)
                    .HasColumnName("newvirus_admin")
                    .HasMaxLength(64)
                    .IsUnicode(false)
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.PolicyName)
                    .IsRequired()
                    .HasColumnName("policy_name")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("''");

                entity.Property(e => e.SaUserconf)
                    .HasColumnName("sa_userconf")
                    .HasMaxLength(64)
                    .IsUnicode(false)
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.SaUsername)
                    .HasColumnName("sa_username")
                    .HasMaxLength(64)
                    .IsUnicode(false)
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.SpamAdmin)
                    .HasColumnName("spam_admin")
                    .HasMaxLength(64)
                    .IsUnicode(false)
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.SpamDsnCutoffLevel)
                    .HasColumnName("spam_dsn_cutoff_level")
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.SpamKillLevel)
                    .HasColumnName("spam_kill_level")
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.SpamLover)
                    .HasColumnName("spam_lover")
                    .HasColumnType("char(1)")
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.SpamQuarantineCutoffLevel)
                    .HasColumnName("spam_quarantine_cutoff_level")
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.SpamQuarantineTo)
                    .HasColumnName("spam_quarantine_to")
                    .HasMaxLength(64)
                    .IsUnicode(false)
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.SpamSubjectTag)
                    .HasColumnName("spam_subject_tag")
                    .HasMaxLength(64)
                    .IsUnicode(false)
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.SpamSubjectTag2)
                    .HasColumnName("spam_subject_tag2")
                    .HasMaxLength(64)
                    .IsUnicode(false)
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.SpamSubjectTag3)
                    .HasColumnName("spam_subject_tag3")
                    .HasMaxLength(64)
                    .IsUnicode(false)
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.SpamTag2Level)
                    .HasColumnName("spam_tag2_level")
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.SpamTag3Level)
                    .HasColumnName("spam_tag3_level")
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.SpamTagLevel)
                    .HasColumnName("spam_tag_level")
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.UncheckedLover)
                    .HasColumnName("unchecked_lover")
                    .HasColumnType("char(1)")
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.UncheckedQuarantineTo)
                    .HasColumnName("unchecked_quarantine_to")
                    .HasMaxLength(64)
                    .IsUnicode(false)
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.VirusAdmin)
                    .HasColumnName("virus_admin")
                    .HasMaxLength(64)
                    .IsUnicode(false)
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.VirusLover)
                    .HasColumnName("virus_lover")
                    .HasColumnType("char(1)")
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.VirusQuarantineTo)
                    .HasColumnName("virus_quarantine_to")
                    .HasMaxLength(64)
                    .IsUnicode(false)
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.Warnbadhrecip)
                    .HasColumnName("warnbadhrecip")
                    .HasColumnType("char(1)")
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.Warnbannedrecip)
                    .HasColumnName("warnbannedrecip")
                    .HasColumnType("char(1)")
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.Warnvirusrecip)
                    .HasColumnName("warnvirusrecip")
                    .HasColumnType("char(1)")
                    .HasDefaultValueSql("NULL");
            });

            modelBuilder.Entity<Quarantine>(entity =>
            {
                entity.HasKey(e => new { e.PartitionTag, e.MailId, e.ChunkInd });

                entity.ToTable("quarantine", "amavisd");

                entity.HasIndex(e => e.MailId)
                    .HasName("quar_idx_mail_id");

                entity.Property(e => e.PartitionTag)
                    .HasColumnName("partition_tag")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.MailId)
                    .HasColumnName("mail_id")
                    .HasColumnType("varbinary(16)");

                entity.Property(e => e.ChunkInd)
                    .HasColumnName("chunk_ind")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.MailText)
                    .IsRequired()
                    .HasColumnName("mail_text")
                    .HasColumnType("blob");
            });

            modelBuilder.Entity<Users>(entity =>
            {
                entity.ToTable("users", "amavisd");

                entity.HasIndex(e => e.Email)
                    .HasName("email")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasColumnName("email")
                    .HasColumnType("varbinary(255)");

                entity.Property(e => e.Fullname)
                    .HasColumnName("fullname")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("NULL");

                entity.Property(e => e.PolicyId)
                    .HasColumnName("policy_id")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.Priority)
                    .HasColumnName("priority")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("7");
            });

            modelBuilder.Entity<Wblist>(entity =>
            {
                entity.HasKey(e => new { e.Rid, e.Sid });

                entity.ToTable("wblist", "amavisd");

                entity.Property(e => e.Rid)
                    .HasColumnName("rid")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Sid)
                    .HasColumnName("sid")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Wb)
                    .IsRequired()
                    .HasColumnName("wb")
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });
        }
    }
}
