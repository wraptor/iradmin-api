﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.Extensions.Configuration;

using iradmin_backend.Models;

namespace iradmin_backend.Contexts
{
    public partial class iredapdContext : DbContext
    {
        public iredapdContext(DbContextOptions<iredapdContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Greylisting> Greylisting { get; set; }
        public virtual DbSet<GreylistingTracking> GreylistingTracking { get; set; }
        public virtual DbSet<GreylistingWhitelistDomains> GreylistingWhitelistDomains { get; set; }
        public virtual DbSet<GreylistingWhitelistDomainSpf> GreylistingWhitelistDomainSpf { get; set; }
        public virtual DbSet<GreylistingWhitelists> GreylistingWhitelists { get; set; }
        public virtual DbSet<Throttle> Throttle { get; set; }
        public virtual DbSet<ThrottleTracking> ThrottleTracking { get; set; }
        public virtual DbSet<WblistRdns> WblistRdns { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Greylisting>(entity =>
            {
                entity.ToTable("greylisting", "iredapd");

                entity.HasIndex(e => e.Comment)
                    .HasName("comment");

                entity.HasIndex(e => new { e.Account, e.Sender })
                    .HasName("account")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20) unsigned");

                entity.Property(e => e.Account)
                    .IsRequired()
                    .HasColumnName("account")
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasDefaultValueSql("''");

                entity.Property(e => e.Active)
                    .HasColumnName("active")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.Comment)
                    .IsRequired()
                    .HasColumnName("comment")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("''");

                entity.Property(e => e.Priority)
                    .HasColumnName("priority")
                    .HasColumnType("tinyint(2)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Sender)
                    .IsRequired()
                    .HasColumnName("sender")
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasDefaultValueSql("''");

                entity.Property(e => e.SenderPriority)
                    .HasColumnName("sender_priority")
                    .HasColumnType("tinyint(2)")
                    .HasDefaultValueSql("0");
            });

            modelBuilder.Entity<GreylistingTracking>(entity =>
            {
                entity.ToTable("greylisting_tracking", "iredapd");

                entity.HasIndex(e => e.RcptDomain)
                    .HasName("rcpt_domain");

                entity.HasIndex(e => e.SenderDomain)
                    .HasName("sender_domain");

                entity.HasIndex(e => new { e.ClientAddress, e.Passed })
                    .HasName("client_address_passed");

                entity.HasIndex(e => new { e.Sender, e.Recipient, e.ClientAddress })
                    .HasName("sender")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20) unsigned");

                entity.Property(e => e.BlockExpired)
                    .HasColumnName("block_expired")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.BlockedCount)
                    .HasColumnName("blocked_count")
                    .HasColumnType("bigint(20)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.ClientAddress)
                    .IsRequired()
                    .HasColumnName("client_address")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.InitTime)
                    .HasColumnName("init_time")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Passed)
                    .HasColumnName("passed")
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.RcptDomain)
                    .IsRequired()
                    .HasColumnName("rcpt_domain")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("''");

                entity.Property(e => e.Recipient)
                    .IsRequired()
                    .HasColumnName("recipient")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.RecordExpired)
                    .HasColumnName("record_expired")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Sender)
                    .IsRequired()
                    .HasColumnName("sender")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.SenderDomain)
                    .IsRequired()
                    .HasColumnName("sender_domain")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("''");
            });

            modelBuilder.Entity<GreylistingWhitelistDomains>(entity =>
            {
                entity.ToTable("greylisting_whitelist_domains", "iredapd");

                entity.HasIndex(e => e.Domain)
                    .HasName("domain")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20) unsigned");

                entity.Property(e => e.Domain)
                    .IsRequired()
                    .HasColumnName("domain")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("''");
            });

            modelBuilder.Entity<GreylistingWhitelistDomainSpf>(entity =>
            {
                entity.ToTable("greylisting_whitelist_domain_spf", "iredapd");

                entity.HasIndex(e => e.Comment)
                    .HasName("comment");

                entity.HasIndex(e => new { e.Account, e.Sender })
                    .HasName("account")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20) unsigned");

                entity.Property(e => e.Account)
                    .IsRequired()
                    .HasColumnName("account")
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasDefaultValueSql("''");

                entity.Property(e => e.Comment)
                    .IsRequired()
                    .HasColumnName("comment")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("''");

                entity.Property(e => e.Sender)
                    .IsRequired()
                    .HasColumnName("sender")
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasDefaultValueSql("''");
            });

            modelBuilder.Entity<GreylistingWhitelists>(entity =>
            {
                entity.ToTable("greylisting_whitelists", "iredapd");

                entity.HasIndex(e => e.Comment)
                    .HasName("comment");

                entity.HasIndex(e => new { e.Account, e.Sender })
                    .HasName("account")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20) unsigned");

                entity.Property(e => e.Account)
                    .IsRequired()
                    .HasColumnName("account")
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasDefaultValueSql("''");

                entity.Property(e => e.Comment)
                    .IsRequired()
                    .HasColumnName("comment")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("''");

                entity.Property(e => e.Sender)
                    .IsRequired()
                    .HasColumnName("sender")
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasDefaultValueSql("''");
            });

            modelBuilder.Entity<Throttle>(entity =>
            {
                entity.ToTable("throttle", "iredapd");

                entity.HasIndex(e => new { e.Account, e.Kind })
                    .HasName("account_kind")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20) unsigned");

                entity.Property(e => e.Account)
                    .IsRequired()
                    .HasColumnName("account")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Kind)
                    .IsRequired()
                    .HasColumnName("kind")
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasDefaultValueSql("'outbound'");

                entity.Property(e => e.MaxMsgs)
                    .HasColumnName("max_msgs")
                    .HasColumnType("bigint(20)")
                    .HasDefaultValueSql("-1");

                entity.Property(e => e.MaxQuota)
                    .HasColumnName("max_quota")
                    .HasColumnType("bigint(20)")
                    .HasDefaultValueSql("-1");

                entity.Property(e => e.MsgSize)
                    .HasColumnName("msg_size")
                    .HasColumnType("bigint(20)")
                    .HasDefaultValueSql("-1");

                entity.Property(e => e.Period)
                    .HasColumnName("period")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Priority)
                    .HasColumnName("priority")
                    .HasColumnType("tinyint(1) unsigned")
                    .HasDefaultValueSql("0");
            });

            modelBuilder.Entity<ThrottleTracking>(entity =>
            {
                entity.ToTable("throttle_tracking", "iredapd");

                entity.HasIndex(e => new { e.Tid, e.Account })
                    .HasName("tid_account")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20) unsigned");

                entity.Property(e => e.Account)
                    .IsRequired()
                    .HasColumnName("account")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("''");

                entity.Property(e => e.CurMsgs)
                    .HasColumnName("cur_msgs")
                    .HasColumnType("mediumint(8) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.CurQuota)
                    .HasColumnName("cur_quota")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.InitTime)
                    .HasColumnName("init_time")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.LastTime)
                    .HasColumnName("last_time")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Period)
                    .HasColumnName("period")
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Tid)
                    .HasColumnName("tid")
                    .HasColumnType("bigint(20) unsigned")
                    .HasDefaultValueSql("0");

                entity.HasOne(d => d.T)
                    .WithMany(p => p.ThrottleTracking)
                    .HasForeignKey(d => d.Tid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("throttle_tracking_ibfk_1");
            });

            modelBuilder.Entity<WblistRdns>(entity =>
            {
                entity.ToTable("wblist_rdns", "iredapd");

                entity.HasIndex(e => e.Rdns)
                    .HasName("rdns")
                    .IsUnique();

                entity.HasIndex(e => e.Wb)
                    .HasName("wb");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("bigint(20) unsigned");

                entity.Property(e => e.Rdns)
                    .IsRequired()
                    .HasColumnName("rdns")
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasDefaultValueSql("''");

                entity.Property(e => e.Wb)
                    .IsRequired()
                    .HasColumnName("wb")
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasDefaultValueSql("'B'");
            });
        }
    }
}
