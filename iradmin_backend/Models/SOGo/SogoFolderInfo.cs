﻿using System;
using System.Collections.Generic;

namespace iradmin_backend.Models
{
    public partial class SogoFolderInfo
    {
        public long CFolderId { get; set; }
        public string CPath { get; set; }
        public string CPath1 { get; set; }
        public string CPath2 { get; set; }
        public string CPath3 { get; set; }
        public string CPath4 { get; set; }
        public string CFoldername { get; set; }
        public string CLocation { get; set; }
        public string CQuickLocation { get; set; }
        public string CAclLocation { get; set; }
        public string CFolderType { get; set; }
    }
}
