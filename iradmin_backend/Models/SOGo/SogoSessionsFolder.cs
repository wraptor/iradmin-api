﻿using System;
using System.Collections.Generic;

namespace iradmin_backend.Models
{
    public partial class SogoSessionsFolder
    {
        public string CId { get; set; }
        public string CValue { get; set; }
        public int CCreationdate { get; set; }
        public int CLastseen { get; set; }
    }
}
