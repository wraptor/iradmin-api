﻿using System;
using System.Collections.Generic;

namespace iradmin_backend.Models
{
    public partial class SogoQuickContact
    {
        public int CFolderId { get; set; }
        public string CName { get; set; }
        public string CGivenname { get; set; }
        public string CCn { get; set; }
        public string CSn { get; set; }
        public string CScreenname { get; set; }
        public string CL { get; set; }
        public string CMail { get; set; }
        public string CO { get; set; }
        public string COu { get; set; }
        public string CTelephonenumber { get; set; }
        public string CCategories { get; set; }
        public string CComponent { get; set; }
        public int? CHascertificate { get; set; }
    }
}
