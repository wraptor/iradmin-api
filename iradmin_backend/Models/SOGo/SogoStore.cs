﻿using System;
using System.Collections.Generic;

namespace iradmin_backend.Models
{
    public partial class SogoStore
    {
        public int CFolderId { get; set; }
        public string CName { get; set; }
        public string CContent { get; set; }
        public int CCreationdate { get; set; }
        public int CLastmodified { get; set; }
        public int CVersion { get; set; }
        public int? CDeleted { get; set; }
    }
}
