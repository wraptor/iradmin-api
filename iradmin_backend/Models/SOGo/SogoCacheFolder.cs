﻿using System;
using System.Collections.Generic;

namespace iradmin_backend.Models
{
    public partial class SogoCacheFolder
    {
        public string CUid { get; set; }
        public string CPath { get; set; }
        public string CParentPath { get; set; }
        public byte CType { get; set; }
        public int CCreationdate { get; set; }
        public int CLastmodified { get; set; }
        public int CVersion { get; set; }
        public byte CDeleted { get; set; }
        public string CContent { get; set; }
    }
}
