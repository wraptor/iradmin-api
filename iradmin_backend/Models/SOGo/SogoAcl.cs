﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace iradmin_backend.Models
{
    public partial class SogoAcl
    {
        public int CFolderId { get; set; }
        public string CObject { get; set; }
        public string CUid { get; set; }
        public string CRole { get; set; }
    }
}
