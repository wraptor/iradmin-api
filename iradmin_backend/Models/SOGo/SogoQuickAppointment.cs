﻿using System;
using System.Collections.Generic;

namespace iradmin_backend.Models
{
    public partial class SogoQuickAppointment
    {
        public int CFolderId { get; set; }
        public string CName { get; set; }
        public string CUid { get; set; }
        public int? CStartdate { get; set; }
        public int? CEnddate { get; set; }
        public int? CCycleenddate { get; set; }
        public string CTitle { get; set; }
        public string CParticipants { get; set; }
        public int? CIsallday { get; set; }
        public int? CIscycle { get; set; }
        public string CCycleinfo { get; set; }
        public int CClassification { get; set; }
        public int CIsopaque { get; set; }
        public int CStatus { get; set; }
        public int? CPriority { get; set; }
        public string CLocation { get; set; }
        public string COrgmail { get; set; }
        public string CPartmails { get; set; }
        public string CPartstates { get; set; }
        public string CCategory { get; set; }
        public int? CSequence { get; set; }
        public string CComponent { get; set; }
        public int? CNextalarm { get; set; }
        public string CDescription { get; set; }
    }
}
