﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace iradmin_backend.Models
{
    public partial class SogoAlarmsFolder
    {
        public string CPath { get; set; }
        public string CName { get; set; }
        public string CUid { get; set; }
        public int CRecurrenceId { get; set; }
        public int CAlarmNumber { get; set; }
        public int CAlarmDate { get; set; }
    }
}
