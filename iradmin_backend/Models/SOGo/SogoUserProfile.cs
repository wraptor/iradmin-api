﻿using System;
using System.Collections.Generic;

namespace iradmin_backend.Models
{
    public partial class SogoUserProfile
    {
        public string CUid { get; set; }
        public string CDefaults { get; set; }
        public string CSettings { get; set; }
    }
}
