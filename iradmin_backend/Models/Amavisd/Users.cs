﻿using System;
using System.Collections.Generic;

namespace iradmin_backend.Models
{
    public partial class Users
    {
        public int Id { get; set; }
        public int Priority { get; set; }
        public int PolicyId { get; set; }
        public byte[] Email { get; set; }
        public string Fullname { get; set; }
    }
}
