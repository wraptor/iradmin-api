﻿using System;
using System.Collections.Generic;

namespace iradmin_backend.Models
{
    public partial class OutboundWblist
    {
        public int Rid { get; set; }
        public int Sid { get; set; }
        public string Wb { get; set; }
    }
}
