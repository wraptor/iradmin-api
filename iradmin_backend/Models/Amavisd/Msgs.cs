﻿using System;
using System.Collections.Generic;

namespace iradmin_backend.Models
{
    public partial class Msgs
    {
        public int PartitionTag { get; set; }
        public byte[] MailId { get; set; }
        public byte[] SecretId { get; set; }
        public string AmId { get; set; }
        public int TimeNum { get; set; }
        public string TimeIso { get; set; }
        public long Sid { get; set; }
        public string Policy { get; set; }
        public string ClientAddr { get; set; }
        public int Size { get; set; }
        public string Originating { get; set; }
        public string Content { get; set; }
        public string QuarType { get; set; }
        public byte[] QuarLoc { get; set; }
        public string DsnSent { get; set; }
        public float? SpamLevel { get; set; }
        public string MessageId { get; set; }
        public byte[] FromAddr { get; set; }
        public byte[] Subject { get; set; }
        public string Host { get; set; }
    }
}
