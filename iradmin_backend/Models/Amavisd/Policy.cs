﻿using System;
using System.Collections.Generic;

namespace iradmin_backend.Models
{
    public partial class Policy
    {
        public int Id { get; set; }
        public string PolicyName { get; set; }
        public string VirusLover { get; set; }
        public string SpamLover { get; set; }
        public string UncheckedLover { get; set; }
        public string BannedFilesLover { get; set; }
        public string BadHeaderLover { get; set; }
        public string BypassVirusChecks { get; set; }
        public string BypassSpamChecks { get; set; }
        public string BypassBannedChecks { get; set; }
        public string BypassHeaderChecks { get; set; }
        public string VirusQuarantineTo { get; set; }
        public string SpamQuarantineTo { get; set; }
        public string BannedQuarantineTo { get; set; }
        public string UncheckedQuarantineTo { get; set; }
        public string BadHeaderQuarantineTo { get; set; }
        public string CleanQuarantineTo { get; set; }
        public string ArchiveQuarantineTo { get; set; }
        public float? SpamTagLevel { get; set; }
        public float? SpamTag2Level { get; set; }
        public float? SpamTag3Level { get; set; }
        public float? SpamKillLevel { get; set; }
        public float? SpamDsnCutoffLevel { get; set; }
        public float? SpamQuarantineCutoffLevel { get; set; }
        public string AddrExtensionVirus { get; set; }
        public string AddrExtensionSpam { get; set; }
        public string AddrExtensionBanned { get; set; }
        public string AddrExtensionBadHeader { get; set; }
        public string Warnvirusrecip { get; set; }
        public string Warnbannedrecip { get; set; }
        public string Warnbadhrecip { get; set; }
        public string NewvirusAdmin { get; set; }
        public string VirusAdmin { get; set; }
        public string BannedAdmin { get; set; }
        public string BadHeaderAdmin { get; set; }
        public string SpamAdmin { get; set; }
        public string SpamSubjectTag { get; set; }
        public string SpamSubjectTag2 { get; set; }
        public string SpamSubjectTag3 { get; set; }
        public int? MessageSizeLimit { get; set; }
        public string BannedRulenames { get; set; }
        public string DisclaimerOptions { get; set; }
        public string ForwardMethod { get; set; }
        public string SaUserconf { get; set; }
        public string SaUsername { get; set; }
    }
}
