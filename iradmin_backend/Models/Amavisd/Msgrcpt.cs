﻿using System;
using System.Collections.Generic;

namespace iradmin_backend.Models
{
    public partial class Msgrcpt
    {
        public int PartitionTag { get; set; }
        public byte[] MailId { get; set; }
        public int Rseqnum { get; set; }
        public long Rid { get; set; }
        public string IsLocal { get; set; }
        public string Content { get; set; }
        public string Ds { get; set; }
        public string Rs { get; set; }
        public string Bl { get; set; }
        public string Wl { get; set; }
        public float? BspamLevel { get; set; }
        public string SmtpResp { get; set; }
    }
}
