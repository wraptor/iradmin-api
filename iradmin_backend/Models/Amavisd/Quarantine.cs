﻿using System;
using System.Collections.Generic;

namespace iradmin_backend.Models
{
    public partial class Quarantine
    {
        public int PartitionTag { get; set; }
        public byte[] MailId { get; set; }
        public int ChunkInd { get; set; }
        public byte[] MailText { get; set; }
    }
}
