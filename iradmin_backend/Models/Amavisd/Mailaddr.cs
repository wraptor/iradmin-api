﻿using System;
using System.Collections.Generic;

namespace iradmin_backend.Models
{
    public partial class Mailaddr
    {
        public int Id { get; set; }
        public int Priority { get; set; }
        public byte[] Email { get; set; }
    }
}
