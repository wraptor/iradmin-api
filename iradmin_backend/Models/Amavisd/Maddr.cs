﻿using System;
using System.Collections.Generic;

namespace iradmin_backend.Models
{
    public partial class Maddr
    {
        public int? PartitionTag { get; set; }
        public long Id { get; set; }
        public byte[] Email { get; set; }
        public byte[] EmailRaw { get; set; }
        public string Domain { get; set; }
    }
}
