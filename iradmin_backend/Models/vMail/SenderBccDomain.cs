﻿using System;
using System.Collections.Generic;

namespace iradmin_backend.Models
{
    public partial class SenderBccDomain
    {
        public string Domain { get; set; }
        public string BccAddress { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public DateTime Expired { get; set; }
        public byte Active { get; set; }
    }
}
