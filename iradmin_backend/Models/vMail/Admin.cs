﻿using System;
using System.Collections.Generic;

namespace iradmin_backend.Models
{
    public partial class Admin
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public string Language { get; set; }
        public DateTime Passwordlastchange { get; set; }
        public string Settings { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public DateTime Expired { get; set; }
        public byte Active { get; set; }
    }
}
