﻿using System;
using System.Collections.Generic;

namespace iradmin_backend.Models
{
    public partial class ShareFolder
    {
        public string FromUser { get; set; }
        public string ToUser { get; set; }
        public string Dummy { get; set; }
    }
}
