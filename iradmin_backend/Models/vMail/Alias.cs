﻿using System;
using System.Collections.Generic;

namespace iradmin_backend.Models
{
    public partial class Alias
    {
        public string Address { get; set; }
        public string Name { get; set; }
        public string Accesspolicy { get; set; }
        public string Domain { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public DateTime Expired { get; set; }
        public byte Active { get; set; }
    }
}
