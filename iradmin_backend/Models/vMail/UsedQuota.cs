﻿using System;
using System.Collections.Generic;

namespace iradmin_backend.Models
{
    public partial class UsedQuota
    {
        public string Username { get; set; }
        public long Bytes { get; set; }
        public long Messages { get; set; }
        public string Domain { get; set; }
    }
}
