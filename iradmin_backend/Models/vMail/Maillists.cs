﻿using System;
using System.Collections.Generic;

namespace iradmin_backend.Models
{
    public partial class Maillists
    {
        public long Id { get; set; }
        public string Address { get; set; }
        public string Domain { get; set; }
        public string Transport { get; set; }
        public string Accesspolicy { get; set; }
        public long Maxmsgsize { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Mlid { get; set; }
        public byte IsNewsletter { get; set; }
        public string Settings { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public DateTime Expired { get; set; }
        public byte Active { get; set; }
    }
}
