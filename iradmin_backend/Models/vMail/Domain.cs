﻿using System;
using System.Collections.Generic;

namespace iradmin_backend.Models
{
    public partial class Domain
    {
        public string Domainname { get; set; }
        public string Description { get; set; }
        public string Disclaimer { get; set; }
        public int? Aliases { get; set; }
        public int? Mailboxes { get; set; }
        public int? Maillists { get; set; }
        public long? Maxquota { get; set; }
        public long Quota { get; set; }
        public string Transport { get; set; }
        public byte Backupmx { get; set; }
        public string Settings { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public DateTime Expired { get; set; }
        public byte Active { get; set; }
    }
}
