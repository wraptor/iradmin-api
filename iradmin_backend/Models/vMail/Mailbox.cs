﻿using System;
using System.Collections.Generic;

namespace iradmin_backend.Models
{
    public partial class Mailbox
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public string Language { get; set; }
        public string Storagebasedirectory { get; set; }
        public string Storagenode { get; set; }
        public string Maildir { get; set; }
        public long Quota { get; set; }
        public string Domain { get; set; }
        public string Transport { get; set; }
        public string Department { get; set; }
        public string Rank { get; set; }
        public string Employeeid { get; set; }
        public byte Isadmin { get; set; }
        public byte Isglobaladmin { get; set; }
        public byte Enablesmtp { get; set; }
        public byte Enablesmtpsecured { get; set; }
        public byte Enablepop3 { get; set; }
        public byte Enablepop3secured { get; set; }
        public byte Enablepop3tls { get; set; }
        public byte Enableimap { get; set; }
        public byte Enableimapsecured { get; set; }
        public byte Enableimaptls { get; set; }
        public byte Enabledeliver { get; set; }
        public byte Enablelda { get; set; }
        public byte Enablemanagesieve { get; set; }
        public byte Enablemanagesievesecured { get; set; }
        public byte Enablesieve { get; set; }
        public byte Enablesievesecured { get; set; }
        public byte Enablesievetls { get; set; }
        public byte Enableinternal { get; set; }
        public byte Enabledoveadm { get; set; }
        public byte EnablelibStorage { get; set; }
        public byte EnableindexerWorker { get; set; }
        public byte Enablelmtp { get; set; }
        public byte Enabledsync { get; set; }
        public byte Enablesogo { get; set; }
        public string AllowNets { get; set; }
        public DateTime Lastlogindate { get; set; }
        public int Lastloginipv4 { get; set; }
        public string Lastloginprotocol { get; set; }
        public string Disclaimer { get; set; }
        public string Allowedsenders { get; set; }
        public string Rejectedsenders { get; set; }
        public string Allowedrecipients { get; set; }
        public string Rejectedrecipients { get; set; }
        public string Settings { get; set; }
        public DateTime Passwordlastchange { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public DateTime Expired { get; set; }
        public byte Active { get; set; }
    }
}
