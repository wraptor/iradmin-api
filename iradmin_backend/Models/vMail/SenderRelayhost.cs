﻿using System;
using System.Collections.Generic;

namespace iradmin_backend.Models
{
    public partial class SenderRelayhost
    {
        public long Id { get; set; }
        public string Account { get; set; }
        public string Relayhost { get; set; }
    }
}
