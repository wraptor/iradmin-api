﻿using System;
using System.Collections.Generic;

namespace iradmin_backend.Models
{
    public partial class DomainAdmins
    {
        public string Username { get; set; }
        public string Domain { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public DateTime Expired { get; set; }
        public byte Active { get; set; }
    }
}
