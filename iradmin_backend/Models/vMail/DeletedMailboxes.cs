﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace iradmin_backend.Models
{
    public partial class DeletedMailboxes
    {
        public ulong Id { get; set; }
        public DateTime Timestamp { get; set; }
        public string Username { get; set; }
        public string Domain { get; set; }
        public string Maildir { get; set; }
        public string Admin { get; set; }
        public DateTime DeleteDate { get; set; }
    }
}
