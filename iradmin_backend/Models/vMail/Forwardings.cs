﻿using System;
using System.Collections.Generic;

namespace iradmin_backend.Models
{
    public partial class Forwardings
    {
        public long Id { get; set; }
        public string Address { get; set; }
        public string Forwarding { get; set; }
        public string Domain { get; set; }
        public string DestDomain { get; set; }
        public byte IsMaillist { get; set; }
        public byte IsList { get; set; }
        public byte IsForwarding { get; set; }
        public byte IsAlias { get; set; }
        public byte Active { get; set; }
    }
}
