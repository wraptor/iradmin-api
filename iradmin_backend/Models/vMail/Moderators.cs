﻿using System;
using System.Collections.Generic;

namespace iradmin_backend.Models
{
    public partial class Moderators
    {
        public long Id { get; set; }
        public string Address { get; set; }
        public string Moderator { get; set; }
        public string Domain { get; set; }
        public string DestDomain { get; set; }
    }
}
