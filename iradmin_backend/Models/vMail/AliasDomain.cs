﻿using System;
using System.Collections.Generic;

namespace iradmin_backend.Models
{
    public partial class AliasDomain
    {
        public string AliasDomain1 { get; set; }
        public string TargetDomain { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public byte Active { get; set; }
    }
}
