using System;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

using iradmin_backend.Contexts;

namespace iradmin_backend.Middleware
{
    public class AuthSessionMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger<AuthSessionMiddleware> _log;

        public AuthSessionMiddleware(RequestDelegate next, ILogger<AuthSessionMiddleware> log)
        {
            _next = next;
            _log = log;    
        }

        public async Task InvokeAsync(HttpContext context, vmailContext vmail)
        {
            await context.Session.LoadAsync();
            string mailAddr = context.Session.GetString(Constants.Session.Mail);
            if(mailAddr != null)
            {
                if(!string.IsNullOrWhiteSpace(mailAddr))
                {
                    Models.Mailbox user = null;
                    try {
                        user = await (from x in vmail.Mailbox
                            where x.Username.ToLower() == mailAddr && x.Active >= 1 && x.Expired > DateTime.UtcNow
                            select new Models.Mailbox() {
                                Isadmin = x.Isadmin,
                                Isglobaladmin = x.Isglobaladmin
                            }).FirstAsync();
                        
                    }
                    catch(InvalidOperationException) { }

                    if(user != null)
                    {
                        context.Items[Constants.General.Role] = (user.Isglobaladmin >= 1 ? 2 : (user.Isadmin>=1 ? 1 : 0) );
                        context.Items[Constants.General.Domain] = mailAddr.ToLower().Split('@')[1];
                    }
                    else
                    {
                        _log.LogInformation("Session using invalid mail detected: {Mail}", mailAddr);
                        await RemoveSession(context);
                    }
                }
                else
                {
                    _log.LogInformation("Session using invalid mail detected: {Mail}", mailAddr);
                    await RemoveSession(context);
                }
            }
            await _next(context);
        }

        public async Task RemoveSession(HttpContext context)
        {
            context.Session.Clear();
            await context.Session.CommitAsync();
            context.Response.Cookies.Delete(Constants.Session.Key);
        }
    }
}