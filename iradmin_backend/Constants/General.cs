using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace iradmin_backend
{
    public partial class Constants
    {
        public class General
        {
            public const string Role = "Role",
                Domain = "Domain";
        }
        public class Logging
        {
            public const string Format = "[{Timestamp:HH:mm:ss zzz}] {Level:u3}: {Message}{NewLine}{Exception}";
        }
        public class Session
        {
            public const string Mail = "Mail",
                Key = "Session";
        }
    }
}