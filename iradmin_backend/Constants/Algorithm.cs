﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace iradmin_backend
{
    public partial class Constants
    {
        public class HashAlgorithms
        {
            public enum Encoding {
                Base64,
                Hex,
                Crypt
            };

            // Encodings
            public const string
                Base64 = "BASE64",
                Base64Short = "B64",
                HEXIdentifier = "HEX",
                Crypt = "CRYPT";

            // Unsalted schema
            public const string
                PlainText = "PLAIN",
                LDAP_MD5 = "LDAP-MD5",
                PLAIN_MD5 = "PLAIN-MD5",
                SHA = "SHA",
                SHA1 = "SHA1",
                SHA256 = "SHA256",
                SHA512 = "SHA512";
            // Salted schemas
            public const string
                SMD5 = "SMD5",
                SSHA = "SSHA",
                SSHA1 = "SSHA1",
                SSHA256 = "SSHA256",
                SSHA512 = "SSHA512";

            // Crypto schema
            public const string
                MD5 = "MD5",
                MD5Crypt = "MD5-CRYPT",
                BCrypt = "BLF-CRYPT",
                SHA256Crypt = "SHA256-CRYPT",
                SHA512Crypt = "SHA512-CRYPT";

            public static readonly Dictionary<string, AlgorithmType>
                Algorithms = new Dictionary<string, AlgorithmType>() {
                    //{ MD5,         new AlgorithmType(32, new MD5CryptoServiceProvider(), Encoding.Crypt) },
                    //{ MD5Crypt,    new AlgorithmType(32, new MD5CryptoServiceProvider(), Encoding.Crypt) },
                    //{ BCrypt,      new AlgorithmType(40, new MD5CryptoServiceProvider(), Encoding.Crypt) },
                    //{ SHA1Crypt,   new AlgorithmType(40, new SHA1CryptoServiceProvider(), Encoding.Crypt) },
                    //{ SHA256Crypt, new AlgorithmType(64, new SHA256CryptoServiceProvider(), Encoding.Crypt) },
                    //{ SHA512Crypt, new AlgorithmType(128, new SHA512CryptoServiceProvider(), Encoding.Crypt) },

                    { LDAP_MD5,  new AlgorithmType(32,  new MD5CryptoServiceProvider()) },
                    { PLAIN_MD5, new AlgorithmType(32,  new MD5CryptoServiceProvider(), Encoding.Hex) },
                    { SMD5,      new AlgorithmType(32,  new MD5CryptoServiceProvider()) },
                    { SHA,       new AlgorithmType(40,  new SHA1CryptoServiceProvider()) },
                    { SHA1,      new AlgorithmType(40,  new SHA1CryptoServiceProvider()) },
                    { SHA256,    new AlgorithmType(64,  new SHA256CryptoServiceProvider()) },
                    { SHA512,    new AlgorithmType(128, new SHA512CryptoServiceProvider()) },
                    { SSHA,      new AlgorithmType(40,  new SHA1CryptoServiceProvider()) },
                    { SSHA1,     new AlgorithmType(40,  new SHA1CryptoServiceProvider()) },
                    { SSHA256,   new AlgorithmType(64,  new SHA256CryptoServiceProvider()) },
                    { SSHA512,   new AlgorithmType(128, new SHA512CryptoServiceProvider()) },
                };

            public static readonly Dictionary<string, string>
                MCFKeys = new Dictionary<string, string>() {
                    { "1", MD5 },

                    { "2", BCrypt },
                    { "2a", BCrypt },
                    { "2x", BCrypt },
                    { "2y", BCrypt },

                    { "5", SHA256Crypt },
                    { "6", SHA512Crypt }
                };
        }
    }

    public class AlgorithmType
    {
        public byte Length { get; private set; }
        public HashAlgorithm Algorithm { get; private set; }
        public bool SaltPrefixed { get; private set; }
        public Constants.HashAlgorithms.Encoding Encoding { get; private set; }

        public AlgorithmType(byte length, HashAlgorithm hash, Constants.HashAlgorithms.Encoding hashEncoding = Constants.HashAlgorithms.Encoding.Base64, bool prependedSalt = false)
        {
            Length = length;
            Algorithm = hash;
            SaltPrefixed = prependedSalt;
            Encoding = hashEncoding;
        }
    }
}
