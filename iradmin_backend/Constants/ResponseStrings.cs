﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace iradmin_backend
{
    public partial class Constants
    {
        public class Responses
        {
            public const string MailRequired = "mail_required";
            public const string MailInvalid = "mail_invalid";

            public const string PasswordRequired = "pass_required";
            public const string PasswordInvalid = "pass_invalid";

            public const string JsonErrorResponse = @"{{""result"":{0}}}";
        }
    }
}
